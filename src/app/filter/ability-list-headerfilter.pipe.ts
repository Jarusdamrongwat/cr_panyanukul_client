import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'abilityListHeaderfilter'
})
export class AbilityListHeaderfilterPipe implements PipeTransform {

  transform(abilities: Array<any>, abilityHeader: string): any {
    // Check If search is Undefined
    if(abilities && abilities.length){
      return abilities.filter(function(ability){
        if(abilityHeader && ability.abilityDetailId.abilityId.abilityHeaderId.abilityHeader.indexOf(abilityHeader) === -1){
          return false;
        }
        return true;
      })
    } else {
      return abilities;
    }
  }

}
