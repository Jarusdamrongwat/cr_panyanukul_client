import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'abilityHeaderFilter'
})
export class AbilityHeaderFilterPipe implements PipeTransform {

  transform(abilities: Array<any>, abilityHeader: string): any {
    // Check If search is Undefined
    if(abilities && abilities.length){
      return abilities.filter(function(ability){
        if(abilityHeader && ability.abilityHeaderId.abilityHeader.indexOf(abilityHeader) === -1){
          return false;
        }
        return true;
      })
    } else {
      return abilities;
    }
  }

}
