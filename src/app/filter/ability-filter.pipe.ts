import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'abilityFilter'
})
export class AbilityFilterPipe implements PipeTransform {

  transform(abilities: Array<any>, abilitySubHeader: string): any {
    // Check If search is Undefined
    if(abilities && abilities.length){
      return abilities.filter(function(ability){
        if(abilitySubHeader && ability.abilityId.abilitySubHeader.indexOf(abilitySubHeader) === -1){
          return false;
        }
        return true;
      })
    } else {
      return abilities;
    }
  }

}
