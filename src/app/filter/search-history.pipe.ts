import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchHistory'
})
export class SearchHistoryPipe implements PipeTransform {

  transform(history: Array<any>, search_classroom: string,
                                 search_dormitory:string): any {
    // Check If search is Undefined
    if(history && history.length){
      return history.filter(function(student){
        if(search_classroom && student.classroom.toLowerCase().indexOf(search_classroom.toLowerCase()) === -1){
          return false;
        }
        if(search_dormitory && student.dormitoryName.toLowerCase().indexOf(search_dormitory.toLowerCase()) === -1){
          return false;
        }
        return true;
      })
    } else {
      return history;
    }
  }

}
