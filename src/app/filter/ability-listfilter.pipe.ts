import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'abilityListfilter'
})
export class AbilityListfilterPipe implements PipeTransform {

  transform(abilities: Array<any>, abilitySubHeader: string): any {
    // Check If search is Undefined
    if(abilities && abilities.length){
      return abilities.filter(function(ability){
        if(abilitySubHeader && ability.abilityDetailId.abilityId.abilitySubHeader.indexOf(abilitySubHeader) === -1){
          return false;
        }
        return true;
      })
    } else {
      return abilities;
    }
  }

}
