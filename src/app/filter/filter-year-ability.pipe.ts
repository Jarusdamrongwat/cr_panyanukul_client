import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterYearAbility'
})
export class FilterYearAbilityPipe implements PipeTransform {

  transform(historyStudent: any[], search_year_ability: string): any {
    if(historyStudent && historyStudent.length){
      return historyStudent.filter(function(student){
        if(search_year_ability && student.year.toString().indexOf(search_year_ability.toString()) === -1){
          return false;
        }
        return true;
      })
    } else {
      return historyStudent;
    }
  }

}
