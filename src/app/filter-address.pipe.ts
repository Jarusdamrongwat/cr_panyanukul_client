import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterAddress'
})
export class FilterAddressPipe implements PipeTransform {

  transform(address: Array<any>, search_district: string,
                                  search_province:string): any {
    // Check If search is Undefined
    if(address && address.length){
      return address.filter(function(student){
        if(search_district && student.district.toLowerCase().indexOf(search_district.toLowerCase()) === -1){
          return false;
        }
        if(search_province && student.province.toLowerCase().indexOf(search_province.toLowerCase()) === -1){
          return false;
        }
        return true;
      })
    } else {
      return address;
    }
  }

}
