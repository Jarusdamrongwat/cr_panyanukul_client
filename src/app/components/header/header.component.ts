import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { Student } from "../../entity/student";
import { StudentService } from 'app/services/student/student.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private _router:Router,
              private _studentService:StudentService) { }

  ngOnInit() {
  }

  // Go to Index Page
  go_to_index_page(){
    this._router.navigate(['/']);
  }

  // Go to Create Page
  go_to_create_page(){
    let student = new Student();
    this._studentService.setter(student);
    this._router.navigate(['/create_student']);
  }

  // Go to Search Page
  go_to_search_page(){
    this._router.navigate(['/search']);
  }

}
