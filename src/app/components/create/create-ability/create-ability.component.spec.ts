import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAbilityComponent } from './create-ability.component';

describe('CreateAbilityComponent', () => {
  let component: CreateAbilityComponent;
  let fixture: ComponentFixture<CreateAbilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAbilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAbilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
