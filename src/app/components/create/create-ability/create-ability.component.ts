import { Component, OnInit } from '@angular/core';
import { Ability } from 'app/entity/ability';

import { AbilityService } from "../../../services/ability/ability.service";
import { AbilityHeader } from 'app/entity/ability-header';
import { AbilityHeaderService } from 'app/services/ability-header/ability-header.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-ability',
  templateUrl: './create-ability.component.html',
  styleUrls: ['./create-ability.component.css']
})
export class CreateAbilityComponent implements OnInit {

  private ability:Ability;
  private ability_header:AbilityHeader;
  private abilityHeaderId:number;

  private header_title:string = "เพิ่มทักษะความสามารถ";

  private progress_bar:number;
  constructor(private _abilityService:AbilityService,
              private _abilityHeaderService:AbilityHeaderService,
              private _router:Router) { }

  ngOnInit() {
    this.ability = this._abilityService.getter();

    this._abilityHeaderService.get_abilitys().subscribe((data)=>{
      this.ability_header = data;
    },(error)=>{
      console.log(error);
    })

    this.progress_bar = 0;
    this.abilityHeaderId = 0;
  }

  add_ability(){
    if(this.ability.abilityId == null) {
      this._abilityService.create_ability(this.ability, this.abilityHeaderId).subscribe((data)=>{
        this.ability = data;
        this._router.navigate(['/create_ability_detail']);
      },(error)=>{
        console.log(error);
      });
    } else {
      this._abilityService.update_ability(this.ability).subscribe((data)=>{
        this.ability = data;
      },(error)=>{
        console.log(error);
      });
    }
  }

}
