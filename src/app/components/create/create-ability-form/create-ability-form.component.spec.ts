import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAbilityFormComponent } from './create-ability-form.component';

describe('CreateAbilityFormComponent', () => {
  let component: CreateAbilityFormComponent;
  let fixture: ComponentFixture<CreateAbilityFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAbilityFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAbilityFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
