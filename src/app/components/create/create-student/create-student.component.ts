import { Component, OnInit } from '@angular/core';

import { CrippleService } from "../../../services/cripple/cripple.service";
import { StudentService } from "../../../services/student/student.service";
import { AddressHomeParticularService } from "../../../services/address-home-particular/address-home-particular.service";
import { CurrentAddressService } from "../../../services/current-address/current-address.service";
import { FamilyMemberService } from "../../../services/family-member/family-member.service";
import { UploadImageService } from "../../../services/upload-image/upload-image.service";

import { Cripple } from "../../../entity/cripple";
import { Student } from "../../../entity/student";
import { FamilyMember } from "../../../entity/family-member";
import { AddressHomeParticular } from "../../../entity/address-home-particular";
import { CurrentAddress } from "../../../entity/current-address";
import { Health } from "../../../entity/health";
import { HistoryStudent } from "../../../entity/history-student";
import { Router, ActivatedRoute } from "@angular/router";

import { CreateAddressComponent } from "../create-address/create-address.component";

import { HttpClient } from '@angular/common/http';
import { HealthService } from 'app/services/health/health.service';
import { HistoryStudentService } from 'app/services/history-student/history-student.service';


@Component({
  selector: 'app-create-student',
  templateUrl: './create-student.component.html',
  styleUrls: ['./create-student.component.css']
})
export class CreateStudentComponent implements OnInit {

  customClass: string = 'customClass';
  isFirstOpen: boolean = true;
  oneAtATime: boolean = true;
  bsValue = new Date();
  url = '';
  readUrl:File;

  private cripples:Cripple;
  private id:Cripple;
  private student:Student;
  private member:FamilyMember;
  private address_home_particular:AddressHomeParticular;
  private address_current:CurrentAddress;
  private health:Health;
  private history_student:HistoryStudent;

  private header_title:string = "เพิ่มข้อมูลนักเรียน";
  private title_student:string = "ข้อมูลนักเรียน";
  private health_student:string = "ข้อมูลสุขภาพนักเรียน";
  private family_student:string = "ข้อมูลครอบครัว";
  private general_student:string = "ข้อมูลทั่วไป";
  public address_student:string = "ที่อยู่ตามทะเบียนบ้าน";
  public current_address_student:string = "ที่อยู่ปัจจุบัน";

  private list_cripple:any;
  private list_province:any;
  private list_district:any;
  private list_sub_district:any;
  private crippleId:any;
  private abilityDropdown:any;
  private progress_bar:number;

  list_prefix = ['เด็กชาย','เด็กหญิง','นาย','นางสาว'];
  list_origin = ['ไทย','อาข่า','ม้ง','มูเซอ','เย้า','กะเหลี่ยง','ลาหู่','จีนฮ่อ','พม่า'];
  list_religion = ['พุทธ','คริส','อิสลาม','ฮินดู'];
  list_status_of_parent = ['อยู่ด้วยกัน','แยกกันอยู่','หย่าร้าง'];
  list_dormitory = ['หอชาย 1','หอชาย 2','หอชาย 3','หอชาย 4','หอชาย 5', 'หอชาย 6', 'หอหญิง 1','หอหญิง 2','หอหญิง 3','หอหญิง 4','หอหญิง 5'];
  list_classroom = [
    'อนุบาล 1','อนุบาล 2','อนุบาล 3', 'ประถมศึกษาปีที่ 1','ประถมศึกษาปีที่ 2','ประถมศึกษาปีที่ 3',
    'ประถมศึกษาปีที่ 4','ประถมศึกษาปีที่ 5','ประถมศึกษาปีที่ 6', 'มัธยมศึกษาปีที่ 1','มัธยมศึกษาปีที่ 2',
    'มัธยมศึกษาปีที่ 3','มัธยมศึกษาปีที่ 4','มัธยมศึกษาปีที่ 5','มัธยมศึกษาปีที่ 6'
  ];

  list_amount = [];
  pass_or_not_pass = ['ผ่าน','ไม่ผ่าน'];

  private list_location:any;

  constructor(private _crippleService:CrippleService,
              private _studentService:StudentService,
              private _addressHomeParticularService:AddressHomeParticularService,
              private _currentAddressService:CurrentAddressService,
              private _familyMemberService:FamilyMemberService,
              private _addressComponent:CreateAddressComponent,
              private _uploadService:UploadImageService,
              private _healthService:HealthService,
              private _historyStudentService:HistoryStudentService,
              private _http:HttpClient,
              private _routerActived:ActivatedRoute,
              private _router:Router) {

                for(let i = 0 ; i < 20 ; i++){
                  this.list_amount.push(i+1);
                }

                this.progress_bar = 0;

                this.list_location = {
                  "CR":{
                    "province":"เชียงราย",
                    "district":[{
                      "districtId":1,
                      "district":"เมืองเชียงราย",
                      "subDistrict":[
                        {
                          "subDistrictId":1,
                          "subDistrict":"ดอยลาน"
                        },
                        {
                          "subDistrictId":2,
                          "subDistrict":"ดอยฮาง"
                        }
                      ]
                    }]
                  }
                };
               }

  ngOnInit() {
    this.student = this._studentService.getter();
    this.address_home_particular = this._addressHomeParticularService.getter();
    this.address_current = this._currentAddressService.getter();
    this.member = this._familyMemberService.getter();
    this.health = this._healthService.getter();
    this.history_student = this._historyStudentService.getter();
    this.crippleId = 0;
    this.list_province = this._addressComponent.list_province;
    this.list_district = this._addressComponent.list_districts;
    this.list_sub_district = this._addressComponent.list_sub_districts;

    this._crippleService.get_cripples().subscribe((cripple)=>{
      this.cripples = cripple;
    },(error)=>{
      console.log(error);
    });

    this.student.image = '';

    // var html = '';
    // for(var key in this.list_location) {
    //   console.log(this.list_location);
    //   html += '<option [value]="">'+this.list_location['CR']['district']+'</option>'
    // }

    // var document = document.getElementById('districtDD');
    // document.innerHtml = html;
  }

  selectFile(event:any){
    if(event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      // Call Once readAsDataUrl is completed
      reader.onload = (event:any) => {
        this.url = event.target.result;
      }
      //Read File As data URL
      reader.readAsDataURL(event.target.files[0]);
      this.readUrl = event.target.files[0];
    }
  }

  add_information(){
    if(this.address_home_particular.homeParticularId == null || this.address_current.currentHomeId == null || this.member.memberId == null){
      // Create Address Home
      this._addressHomeParticularService.create_address_home_particular(this.address_home_particular).subscribe((address_home_particular)=>{
        this.address_home_particular = address_home_particular;
        console.log("Save Address Home Success");
        // Create Current Address
        this._currentAddressService.create_current_address(this.address_current).subscribe((address_current)=>{
          this.address_current = address_current;
          console.log("Save Current Home Success");
          // Create Family Member
          this._familyMemberService.create_family_member(this.member).subscribe((member)=>{
            this.member = member;
            console.log("Save Member Success");
            // Create Student
            this.student.year = this.bsValue.getFullYear();
            this._studentService.create_student(this.student,
                                                this.address_home_particular.homeParticularId,
                                                this.address_current.currentHomeId,
                                                this.crippleId,
                                                this.member.memberId).subscribe((student)=>{
              this.student = student;
              console.log("Save Student Success");
              // Create Health
              this.health.year = this.bsValue.getFullYear();
              this._healthService.create_health(this.health,this.student.studentCode).subscribe((data)=>{
                this.health = data;
                console.log("Save Health Success");
                // Create History Student
                this.history_student.classroom = this.student.currentClassroom;
                this.history_student.year = this.bsValue.getFullYear();
                this._historyStudentService.create_history_student(this.history_student,this.student.studentCode).subscribe((data)=>{
                  this.history_student = data;
                  console.log("Save History Student Success");
                  // Upload Image
                  let formData:FormData = new FormData();
                  formData.append('image', this.readUrl);
                  formData.append('studentCode', this.student.studentCode);
                  this._uploadService.save_image(formData).subscribe((data)=>{
                    console.log(data);
                    this.progress_bar = 100;
                    if(this.history_student.classroom == 'อนุบาล 1' || this.history_student.classroom == 'อนุบาล 2' || this.history_student.classroom == 'อนุบาล 3') {
                      this._router.navigate(['/create_develop_form', this.student.studentCode]);
                    } else {
                      this._router.navigate(['/create_ability_form', this.student.studentCode]);
                    }
                  },(error)=>{
                    console.log(error);
                  });
                },(error)=>{
                  console.log(error);
                });
              },(error)=>{
                console.log(error);
              });
              },(error)=>{
              console.log(error);
              });
          },(error)=>{
            console.log(error);
          });
        },(error)=>{
          console.log(error);
        });
      },(error)=>{
        console.log(error);
      });
    }
  }

}
