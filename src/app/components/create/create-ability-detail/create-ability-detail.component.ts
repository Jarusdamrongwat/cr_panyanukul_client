import { Component, OnInit } from '@angular/core';
import { AbilityDetail } from 'app/entity/ability-detail';
import { Ability } from 'app/entity/ability';
import { AbilityDetailService } from 'app/services/ability-detail/ability-detail.service';
import { AbilityService } from 'app/services/ability/ability.service';

@Component({
  selector: 'app-create-ability-detail',
  templateUrl: './create-ability-detail.component.html',
  styleUrls: ['./create-ability-detail.component.css']
})
export class CreateAbilityDetailComponent implements OnInit {

  private ability:AbilityDetail;
  private abilities:Ability;
  private abilityId:number;

  private header_title:string = "เพิ่มทักษะความสามารถ";

  private progress_bar:number;
  constructor(private _abilityService:AbilityService,
              private _abilityDetailService:AbilityDetailService) { }

  ngOnInit() {
    this.ability = this._abilityDetailService.getter();

    this._abilityService.get_abilitys().subscribe((data)=>{
      this.abilities = data;
    },(error)=>{
      console.log(error);
    })

    this.progress_bar = 0;
    this.abilityId = 0;
  }

  add_ability(){
    if(this.ability.abilityId == null) {
      this._abilityDetailService.create_ability(this.ability, this.abilityId).subscribe((data)=>{
        this.ability = data;
      },(error)=>{
        console.log(error);
      });
    } else {
      this._abilityDetailService.update_ability(this.ability).subscribe((data)=>{
        this.ability = data;
      },(error)=>{
        console.log(error);
      });
    }
  }

}
