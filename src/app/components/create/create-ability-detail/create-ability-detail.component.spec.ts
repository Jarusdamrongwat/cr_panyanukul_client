import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAbilityDetailComponent } from './create-ability-detail.component';

describe('CreateAbilityDetailComponent', () => {
  let component: CreateAbilityDetailComponent;
  let fixture: ComponentFixture<CreateAbilityDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAbilityDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAbilityDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
