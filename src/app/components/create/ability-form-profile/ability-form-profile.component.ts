import { Component, OnInit } from '@angular/core';
import { Ability } from 'app/entity/ability';
import { AbilityDetail } from 'app/entity/ability-detail';
import { ListAbility } from 'app/entity/list-ability';
import { AbilityHeader } from 'app/entity/ability-header';
import { AbilityHeaderService } from 'app/services/ability-header/ability-header.service';
import { AbilityDetailService } from 'app/services/ability-detail/ability-detail.service';
import { ListAbilityService } from 'app/services/list-ability/list-ability.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AbilityService } from 'app/services/ability/ability.service';

@Component({
  selector: 'app-ability-form-profile',
  templateUrl: './ability-form-profile.component.html',
  styleUrls: ['./ability-form-profile.component.css']
})
export class AbilityFormProfileComponent implements OnInit {

  private abilities:Ability[];
  private abilityDetails:AbilityDetail[];
  private selectedAbilityDetail:AbilityDetail;
  private abilityHeaders:AbilityHeader[];
  private studentCode:string;

  private abilityDetail = [];
  private abilityId = [];
  private abilitySelected:any;
  private date = new Date();

  private header_title:string = "เพิ่มข้อมูลนักเรียน";

  private list_ability = new ListAbility();

  constructor(private _abilityHeaderService:AbilityHeaderService,
              private _abilityDetailService:AbilityDetailService,
              private _listAbilityService:ListAbilityService,
              private _routerActive:ActivatedRoute,
              private _abilityService:AbilityService,
              private _router:Router) { }

  ngOnInit() {
    this.studentCode = this._routerActive.snapshot.paramMap.get('id');

    this._abilityService.get_abilitys().subscribe((data)=>{
      this.abilities = data;
    },(error)=>{
      console.log(error);
    });

    this._abilityDetailService.get_abilitys().subscribe((data)=>{
      this.abilityDetails = data;
      // console.log(this.abilityDetails)
    },(error)=>{
      console.log(error);
    });

    this._abilityHeaderService.get_abilitys().subscribe((ability)=>{
      this.abilityHeaders = ability;
      // console.log(this.abilityHeaders)
    },(error)=>{
      console.log(error);
    });
  }

  add_information() {
    for(let item of this.abilityDetail) {
      this.list_ability.year = this.date.getFullYear();
      this._listAbilityService.create_ability(this.list_ability, item, this.studentCode).subscribe((data)=>{
        this.list_ability = data;
        this._router.navigate(['/profile', this.studentCode]);
        console.log("Save Ability Success");
      },(error)=>{
        console.log(error);
      })
    }
  }

  toggle(index){
    // console.log(index);
  }

  selectedAbility(value: string){
    this.abilitySelected = value;
  }
}
