import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbilityFormProfileComponent } from './ability-form-profile.component';

describe('AbilityFormProfileComponent', () => {
  let component: AbilityFormProfileComponent;
  let fixture: ComponentFixture<AbilityFormProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbilityFormProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbilityFormProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
