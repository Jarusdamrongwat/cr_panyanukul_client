import { Component, OnInit } from '@angular/core';

import { AddressHomeParticularService } from "../../../services/address-home-particular/address-home-particular.service";
import { CurrentAddressService } from "../../../services/current-address/current-address.service";
import { FamilyMemberService } from "../../../services/family-member/family-member.service";
import { ParentService } from "../../../services/parent/parent.service";

import { AddressHomeParticular } from "../../../entity/address-home-particular";
import { CurrentAddress } from "../../../entity/current-address";
import { FamilyMember } from "../../../entity/family-member";
import { Parent } from "../../../entity/parent";
import { Router, ActivatedRoute } from "@angular/router";
import { CreateAddressComponent } from "../create-address/create-address.component";

@Component({
  selector: 'app-create-father',
  templateUrl: './create-father.component.html',
  styleUrls: ['./create-father.component.css']
})
export class CreateFatherComponent implements OnInit {

  public family_member:FamilyMember;
  public parent:Parent;
  private address_home_particular:AddressHomeParticular;
  private address_current:CurrentAddress;
  private studentCode:string;
  bsValue = new Date();

  public header_title:string = "เพิ่มข้อมูลนักเรียน";
  public address_student:string = "ที่อยู่ตามทะเบียนบ้าน";
  public current_address_student:string = "ที่อยู่ปัจจุบัน";
  public parent_:string = "บิดา";
  public address_parent:string = "ที่อยู่";

  list_prefix = ['นาย'];
  list_origin = ['ไทย','อาข่า','ม้ง','มูเซอ','เย้า','กะเหลี่ยง','ลาหู่','จีนฮ่อ','พม่า'];
  list_religion = ['พุทธ','คริส','อิสลาม','ฮินดู'];
  list_economic_status = ['มีฐานะดีมาก','มีฐานะดี','พอมีพอกิน','ยากจน'];

  list_amount = [];
  list_province = [];
  list_district = [];
  list_sub_district = [];

  constructor(private _addressHomeParticularService:AddressHomeParticularService,
    private _addressCurrentService:CurrentAddressService,
    private _familyMemberService:FamilyMemberService,
    private _parentService:ParentService,
    private _routerActive:ActivatedRoute,
    private _addressComponent:CreateAddressComponent,
    private _router:Router) {
    for(let i = 0 ; i < 20 ; i++){
      this.list_amount.push(i+1);
    }
   }

  ngOnInit() {
    this.address_home_particular = this._addressHomeParticularService.getter();
    this.address_current = this._addressCurrentService.getter();
    this.family_member = this._familyMemberService.getter();
    this.parent = this._parentService.getter();
    this.list_province = this._addressComponent.list_province;
    this.list_district = this._addressComponent.list_districts;
    this.list_sub_district = this._addressComponent.list_sub_districts;

    this.parent.type = "father";
    this.parent.relationship = "บิดา";
    this.studentCode = this._routerActive.snapshot.paramMap.get('id');
  }

  add_information(){
    if(this.parent.parentId == null){
      // Create Current Address
      this._addressCurrentService.create_current_address(this.address_current).subscribe((current)=>{
        this.address_current = current;
        console.log("Save Current Address Success");
        this.parent.year = this.bsValue.getFullYear();
        this._parentService.create_parent(this.parent, this.address_current.currentHomeId, this.studentCode).subscribe((parent)=>{
          this.parent = parent;
          console.log("Save Parent Success");
          this._router.navigate(['/create_parent',this.studentCode]);
        },(error)=>{
          console.log(error);
          this._router.navigate(['/create_father',this.studentCode]);
        });
      },(error)=>{
        console.log(error);
        this._router.navigate(['/create_father',this.studentCode]);
      });
    }
  }

}
