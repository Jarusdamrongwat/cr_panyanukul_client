import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-address',
  templateUrl: './create-address.component.html',
  styleUrls: ['./create-address.component.css']
})
export class CreateAddressComponent implements OnInit {

  list_province = [
    {id:"CR", province:"เชียงราย"},
    {id:"CM", province:"เชียงใหม่"},
    {id:"PR", province:"แพร่"},
    {id:"PY", province:"พะเยา"},
    {id:"PC", province:"เพชรบูรณ์"},
    {id:"MH", province:"แม่ฮ่องสอน"},
    {id:"UT", province:"อุทัยธานี"},
  ]
  list_districts = [
    {id: 1 , district: 'เมืองเชียงราย', provinceId:'CR'},
    {id: 2 , district: 'เชียงของ', provinceId:'CR'},
    {id: 3 , district: 'เชียงแสน', provinceId:'CR'},
    {id: 4 , district: 'แม่สาย', provinceId:'CR'},
    {id: 5 , district: 'แม่จัน', provinceId:'CR'},
    {id: 6 , district: 'เทิง', provinceId:'CR'},
    {id: 7 , district: 'เวียงแก่น', provinceId:'CR'},
    {id: 8 , district: 'แม่ฟ้าหลวง', provinceId:'CR'},
    {id: 9 , district: 'เวียงป่าเป้า', provinceId:'CR'},
    {id: 10 , district: 'แม่สรวย', provinceId:'CR'},
    {id: 11 , district: 'แม่ลาว', provinceId:'CR'},
    {id: 12 , district: 'พาน', provinceId:'CR'},
    {id: 13 , district: 'เวียงชัย', provinceId:'CR'},
    {id: 14 , district: 'พญาเม็งราย', provinceId:'CR'},
    {id: 15 , district: 'ดอยหลวง', provinceId:'CR'},
    {id: 16 , district: 'ป่าแดด', provinceId:'CR'},
    {id: 17 , district: 'ขุนตาล', provinceId:'CR'},
    {id: 18 , district: 'เวียงเชียงรุ้ง', provinceId:'CR'},
  ];

  list_sub_districts = [
    // อำเภอเมืองเชียงราย
    {id: 1 , sub_district: 'ดอยลาน', districtId:1},
    {id: 2 , sub_district: 'ดอยฮาง', districtId:1},
    {id: 3 , sub_district: 'ท่าสาย', districtId:1},
    {id: 4 , sub_district: 'ท่าสุด', districtId:1},
    {id: 5 , sub_district: 'นางแล', districtId:1},
    {id: 6 , sub_district: 'บ้านดู่', districtId:1},
    {id: 7 , sub_district: 'ป่าอ้อดอนชัย', districtId:1},
    {id: 8 , sub_district: 'รอบเวียง', districtId:1},
    {id: 9 , sub_district: 'ริมกก', districtId:1},
    {id: 10 , sub_district: 'สันทราย', districtId:1},
    {id: 11 , sub_district: 'ห้วยชมภู', districtId:1},
    {id: 12 , sub_district: 'ห้วยสัก', districtId:1},
    {id: 13 , sub_district: 'เวียง', districtId:1},
    {id: 14 , sub_district: 'แม่กรณ์', districtId:1},
    {id: 15 , sub_district: 'แม่ข้าวต้ม', districtId:1},
    // อำเภอเชียงของ
    {id: 16 , sub_district: 'บุญเรือง', districtId:2},
    {id: 17 , sub_district: 'ริมโขง', districtId:2},
    {id: 18 , sub_district: 'ศรีดอนชัย', districtId:2},
    {id: 19 , sub_district: 'สถาน', districtId:2},
    {id: 20 , sub_district: 'ห้วยซ้อ', districtId:2},
    {id: 21 , sub_district: 'เวียง', districtId:2},
    {id: 22 , sub_district: 'ครึ่ง', districtId:2},
    // อำเภอเชียงแสน
    {id: 23 , sub_district: 'บ้านแซว', districtId:3},
    {id: 24 , sub_district: 'ป่าสัก', districtId:3},
    {id: 25 , sub_district: 'ศรีดอนมูล', districtId:3},
    {id: 26 , sub_district: 'เวียง', districtId:3},
    {id: 27 , sub_district: 'แม่เงิน', districtId:3},
    {id: 28 , sub_district: 'โยนก', districtId:3},
    // อำเภอแม่สาย
    {id: 29 , sub_district: 'บ้านด้าย', districtId:4},
    {id: 30 , sub_district: 'เมืองชุม', districtId:4},
    {id: 31 , sub_district: 'ห้วยไคร้', districtId:4},
    {id: 32 , sub_district: 'เกาะช้าง', districtId:4},
    {id: 33 , sub_district: 'เวียงพางคำ', districtId:4},
    {id: 34 , sub_district: 'แม่สาย', districtId:4},
    {id: 35 , sub_district: 'โป่งงาม', districtId:4},
    {id: 36 , sub_district: 'โป่งผา', districtId:4},
    // อำเภอแม่จัน
    {id: 37 , sub_district: 'จอมสวรรค์', districtId:5},
    {id: 38 , sub_district: 'จันจว้า', districtId:5},
    {id: 39 , sub_district: 'จันจว้าใต้', districtId:5},
    {id: 40 , sub_district: 'ท่าข้าวเปลือก', districtId:5},
    {id: 41 , sub_district: 'ป่าซาง', districtId:5},
    {id: 42 , sub_district: 'ป่าตึง', districtId:5},
    {id: 43 , sub_district: 'ศรีค้ำ', districtId:5},
    {id: 44 , sub_district: 'สันทราย', districtId:5},
    {id: 45 , sub_district: 'แม่คำ', districtId:5},
    {id: 46 , sub_district: 'แม่จัน', districtId:5},
    {id: 47 , sub_district: 'แม่ไร่', districtId:5},
    // อำเภอเทิง
    {id: 48 , sub_district: 'งิ้ว', districtId:6},
    {id: 49 , sub_district: 'ตับเต่า', districtId:6},
    {id: 50 , sub_district: 'ปล้อง', districtId:6},
    {id: 51 , sub_district: 'ศรีดอนชัย', districtId:6},
    {id: 52 , sub_district: 'สันทรายงาม', districtId:6},
    {id: 53 , sub_district: 'หงาว', districtId:6},
    {id: 54 , sub_district: 'หนองแรด', districtId:6},
    {id: 55 , sub_district: 'เชียงเคี่ยน', districtId:6},
    {id: 56 , sub_district: 'เวียง', districtId:6},
    {id: 57 , sub_district: 'แม่ลอย', districtId:6},
    // อำเภอเวียงแก่น
    {id: 58 , sub_district: 'ท่าข้าม', districtId:7},
    {id: 59 , sub_district: 'ปอ', districtId:7},
    {id: 60 , sub_district: 'ม่วงยาย', districtId:7},
    {id: 61 , sub_district: 'หล่ายงาว', districtId:7},
    // อำเภอแม่ฟ้าหลวง
    {id: 62 , sub_district: 'ทอดไทย', districtId:8},
    {id: 63 , sub_district: 'แม่ฟ้าหลวง', districtId:8},
    {id: 64 , sub_district: 'แม่สลองนอก', districtId:8},
    {id: 65 , sub_district: 'แม่สลองใน', districtId:8},
    // อำเภอเวียงป่าเป้า
    {id: 66 , sub_district: 'บ้านโป่ง', districtId:9},
    {id: 67 , sub_district: 'ป่างิ้ว', districtId:9},
    {id: 68 , sub_district: 'สันสลี', districtId:9},
    {id: 69 , sub_district: 'เวียง', districtId:9},
    {id: 70 , sub_district: 'เวียงกาหลง', districtId:9},
    {id: 71 , sub_district: 'แม่เจดีย์', districtId:9},
    {id: 72 , sub_district: 'แม่เจดีย์ใหม่', districtId:9},
    // อำเภอแม่สรวย
    {id: 73 , sub_district: 'ท่าก๊อ', districtId:10},
    {id: 74 , sub_district: 'ป่าแดด', districtId:10},
    {id: 75 , sub_district: 'วาวี', districtId:10},
    {id: 76 , sub_district: 'ศรีถ้อย', districtId:10},
    {id: 77 , sub_district: 'เจดีย์หลวง', districtId:10},
    {id: 78 , sub_district: 'แม่พริก', districtId:10},
    {id: 79 , sub_district: 'แม่สรวย', districtId:10},
    // อำเภอแม่ลาว
    {id: 80 , sub_district: 'จอมหมอกแก้ว', districtId:11},
    {id: 81 , sub_district: 'ดงมะดะ', districtId:11},
    {id: 82 , sub_district: 'บัวสลี', districtId:11},
    {id: 83 , sub_district: 'ป่าก่อดำ', districtId:11},
    {id: 84 , sub_district: 'โป่งแพร่', districtId:11},
    // อำเภอพาน
    {id: 85 , sub_district: 'ดอยงาม', districtId:12},
    {id: 86 , sub_district: 'ทรายขาว', districtId:12},
    {id: 87 , sub_district: 'ทานตะวัน', districtId:12},
    {id: 88 , sub_district: 'ธารทอง', districtId:12},
    {id: 89 , sub_district: 'ป่าหุ่ง', districtId:12},
    {id: 90 , sub_district: 'ม่วงคำ', districtId:12},
    {id: 91 , sub_district: 'สันกลาง', districtId:12},
    {id: 92 , sub_district: 'สันติสุข', districtId:12},
    {id: 93 , sub_district: 'สันมะเค็ด', districtId:12},
    {id: 94 , sub_district: 'หัวงัม', districtId:12},
    {id: 95 , sub_district: 'เจริญเมือง', districtId:12},
    {id: 96 , sub_district: 'เมืองพาน', districtId:12},
    {id: 97 , sub_district: 'เวียงห้าว', districtId:12},
    {id: 98 , sub_district: 'แม่อ้อ', districtId:12},
    {id: 99 , sub_district: 'แม่เย็น', districtId:12},
    // อำเภอเวียงชัย
    {id: 100 , sub_district: 'ดอนศิลา', districtId:13},
    {id: 101 , sub_district: 'ผางาม', districtId:13},
    {id: 102 , sub_district: 'เมืองชุม', districtId:13},
    {id: 103 , sub_district: 'เวียงชัย', districtId:13},
    {id: 104 , sub_district: 'เวียงเหนือ', districtId:13},
    // อำเภอพญาเม็งราย
    {id: 105 , sub_district: 'ตาดควัน', districtId:14},
    {id: 106 , sub_district: 'เม็งราย', districtId:14},
    {id: 107 , sub_district: 'แม่ต๋ำ', districtId:14},
    {id: 108 , sub_district: 'แม่เปา', districtId:14},
    {id: 109 , sub_district: 'ไม้ยา', districtId:14},
    // อำเภอดอยหลวง
    {id: 110 , sub_district: 'ปงน้อย', districtId:15},
    {id: 111 , sub_district: 'หนองป่าก่อ', districtId:15},
    {id: 112 , sub_district: 'โชคชัย', districtId:15},
    // อำเภอป่าแดด
    {id: 113 , sub_district: 'ป่าแงะ', districtId:16},
    {id: 114 , sub_district: 'ป่าแดด', districtId:16},
    {id: 115 , sub_district: 'ศรีโพธิ์เงิน', districtId:16},
    {id: 116 , sub_district: 'สันมะค่า', districtId:16},
    {id: 117 , sub_district: 'โรงช้าง', districtId:16},
    // อำเภอขุนตาล
    {id: 118 , sub_district: 'ต้า', districtId:17},
    {id: 119 , sub_district: 'ป่าตาล', districtId:17},
    {id: 120 , sub_district: 'ยางฮอม', districtId:17},
    // อำเภอเวียงเชียงรุ้ง
    {id: 121 , sub_district: 'ดงมหาวัน', districtId:18},
    {id: 122 , sub_district: 'ทุ่งก่อ', districtId:18},
    {id: 123 , sub_district: 'ป่าซาง', districtId:18},
  ];

  ngOnInit() {

  }
}
