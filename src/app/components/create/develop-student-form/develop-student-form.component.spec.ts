import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevelopStudentFormComponent } from './develop-student-form.component';

describe('DevelopStudentFormComponent', () => {
  let component: DevelopStudentFormComponent;
  let fixture: ComponentFixture<DevelopStudentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevelopStudentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevelopStudentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
