import { Component, OnInit } from '@angular/core';
import { HistoryStudentService } from 'app/services/history-student/history-student.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HistoryStudent } from 'app/entity/history-student';
import { CreateStudentComponent } from "../create-student/create-student.component";

@Component({
  selector: 'app-create-history-student',
  templateUrl: './create-history-student.component.html',
  styleUrls: ['./create-history-student.component.css']
})
export class CreateHistoryStudentComponent implements OnInit {

  private history_student:HistoryStudent;
  private header_title:string = "เพิ่มข้อมูลนักเรียน";
  private personal_info:string = "ข้อมูลส่วนตัว";

  private date = new Date();

  private studentCode:string;
  private list_dormitory = [];
  private list_classroom = [];

  constructor(private _routerActive:ActivatedRoute,
              private _router:Router,
              private _historyService:HistoryStudentService,
              private _createStudent:CreateStudentComponent) { }

  ngOnInit() {
    this.studentCode = this._routerActive.snapshot.paramMap.get('id');

    this.history_student = this._historyService.getter();
    this.list_dormitory = this._createStudent.list_dormitory;
    this.list_classroom = this._createStudent.list_classroom;
  }

  add_information() {
    this.history_student.year = this.date.getFullYear();
    this._historyService.create_history_student(this.history_student,this.studentCode).subscribe((data)=>{
      this.history_student = data;
      this._router.navigate(['/profile', this.history_student.studentCode.studentCode]);
    },(error)=>{
      console.log(error);
    });
  }

}
