import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateHistoryStudentComponent } from './create-history-student.component';

describe('CreateHistoryStudentComponent', () => {
  let component: CreateHistoryStudentComponent;
  let fixture: ComponentFixture<CreateHistoryStudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateHistoryStudentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateHistoryStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
