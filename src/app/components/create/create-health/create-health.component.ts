import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HealthService } from 'app/services/health/health.service';
import { Health } from 'app/entity/health';

@Component({
  selector: 'app-create-health',
  templateUrl: './create-health.component.html',
  styleUrls: ['./create-health.component.css']
})
export class CreateHealthComponent implements OnInit {

  private health:Health;
  private header_title:string = "เพิ่มข้อมูลนักเรียน";
  private health_info:string = "ข้อมูลสุขภาพ";

  private date = new Date();

  private studentCode:string;

  constructor(private _routerActive:ActivatedRoute,
              private _router:Router,
              private _healthService:HealthService) { }

  ngOnInit() {
    this.studentCode = this._routerActive.snapshot.paramMap.get('id');

    this.health = this._healthService.getter();
  }

  add_information() {
    this.health.year = this.date.getFullYear();
    this._healthService.create_health(this.health,this.studentCode).subscribe((data)=>{
      this.health = data;
      this._router.navigate(['/profile', this.health.studentCode.studentCode]);
      console.log("Save Health Success");
    },(error)=>{
      console.log(error);
    })
  }

}
