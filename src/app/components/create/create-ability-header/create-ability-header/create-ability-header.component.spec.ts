import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAbilityHeaderComponent } from './create-ability-header.component';

describe('CreateAbilityHeaderComponent', () => {
  let component: CreateAbilityHeaderComponent;
  let fixture: ComponentFixture<CreateAbilityHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAbilityHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAbilityHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
