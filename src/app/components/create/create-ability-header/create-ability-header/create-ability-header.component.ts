import { Component, OnInit } from '@angular/core';
import { AbilityHeaderService } from 'app/services/ability-header/ability-header.service';
import { AbilityHeader } from 'app/entity/ability-header';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-create-ability-header',
  templateUrl: './create-ability-header.component.html',
  styleUrls: ['./create-ability-header.component.css']
})
export class CreateAbilityHeaderComponent implements OnInit {

  private ability_header:AbilityHeader;
  private header_title:string = "เพิ่มทักษะความสามารถ";

  constructor(private _abilityHeaderService:AbilityHeaderService,
              private _router:Router) { }

  ngOnInit() {
    this.ability_header = this._abilityHeaderService.getter();
  }

  add_ability_header(){
    if(this.ability_header.abilityHeaderId == null) {
      this._abilityHeaderService.create_ability(this.ability_header).subscribe((data)=>{
        this.ability_header = data;
        this._router.navigate(['/create_ability']);
      },(error)=>{
        console.log(error);
      });
    } else {
      this._abilityHeaderService.update_ability(this.ability_header).subscribe((data)=>{
        this.ability_header = data;
      },(error)=>{
        console.log(error);
      });
    }
  }

}
