import { Component, OnInit } from '@angular/core';
import { StudentService } from "../../../services/student/student.service";
import { Student } from "../../../entity/student";
import { Router } from "@angular/router";
import { AddressHomeParticularService } from 'app/services/address-home-particular/address-home-particular.service';
import { AddressHomeParticular } from 'app/entity/address-home-particular';
import { HistoryStudentService } from 'app/services/history-student/history-student.service';
import { HistoryStudent } from 'app/entity/history-student';

@Component({
  selector: 'app-search-information',
  templateUrl: './search-information.component.html',
  styleUrls: ['./search-information.component.css']
})
export class SearchInformationComponent implements OnInit {

  private students:Student[];
  private list_year:Student;
  private historyStudent:HistoryStudent;
  private list_student = [];
  private address:AddressHomeParticular[];
  private date = new Date();
  private year:number;

  private title:string = "ค้นหารายชื่อนักเรียน";
  private search_item = [
    {
      title: "ค้นหาโดยโรคประจำตัว",
      placeholder: "ค้นหารายชื่อโดยโรคประจำตัว"
    },
    {
      title: "ค้นหาโดยประเภทความพิการ",
      placeholder: "ค้นหารายชื่อโดยประเภทความพิการ"
    },
    {
      title: "ค้นหาโดยชื่อ-นามสกุล",
      placeholder: {
        fname: "ค้นหารายชื่อโดยชื่อ",
        lname: "ค้นหารายชื่อโดยนามสกุล"
      }
    }
  ]

  constructor(private _studentService:StudentService,
              private _historyService:HistoryStudentService,
              private _router:Router) { }

  ngOnInit() {

    this._studentService.get_by_year(this.date.getFullYear()).subscribe((student)=>{
      this.students = student;
      this.list_student = student;
      console.log("List All Student Success");
    },(error)=>{
      console.log(error);
    });

    this._studentService.get_students().subscribe((data)=>{
      this.list_year = data;
    },(error)=>{
      console.log(error);
    });

    this.year = this.date.getFullYear();
  }

  // Delete Student
  deleteStudent(student){
    if(confirm('คุณต้องการลบข้อมูลนี้หรือไม่')) {
      this._studentService.delete_student(student.studentCode).subscribe((data)=>{
        this.students.splice(this.students.indexOf(student),1);
        console.log("Delete Success");
      },(error)=>{
        console.log(error);
      });
    } else {
      console.log("Not delete Student");
    }

  }

  // Go to Profile Page
  go_to_profile_page(studentCode:string){
    this._router.navigate(['/profile', studentCode]);
  }

  // Select year
  selectYear(value: number){
    // Get all Ability Detail
    this._studentService.get_by_year(value).subscribe((data)=>{
      this.students = data;
      this.list_student = data;
      console.log("List All Student Success");
    },(error)=>{
      console.log(error);
    });
  }

}
