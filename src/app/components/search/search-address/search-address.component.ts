import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddressHomeParticularService } from 'app/services/address-home-particular/address-home-particular.service';
import { AddressHomeParticular } from 'app/entity/address-home-particular';
import { StudentService } from 'app/services/student/student.service';
import { HistoryStudentService } from 'app/services/history-student/history-student.service';
import { HistoryStudent } from 'app/entity/history-student';

@Component({
  selector: 'app-search-address',
  templateUrl: './search-address.component.html',
  styleUrls: ['./search-address.component.css']
})
export class SearchAddressComponent implements OnInit {

  private address:AddressHomeParticular[];
  private list_year:HistoryStudent;
  private list_address = [];
  private year:number;
  private title:string = "ค้นหารายชื่อนักเรียน";

  bsValue = new Date();

  private search_item = [
    {
      title: "ค้นหาโดยอำเภอ",
      placeholder: "ค้นหารายชื่อโดยอำเภอ"
    },
    {
      title: "ค้นหาโดยจังหวัด",
      placeholder: "ค้นหารายชื่อโดยจังหวัด"
    }
  ]

  constructor(private _addressService:AddressHomeParticularService,
              private _historyService:HistoryStudentService,
              private _studentService:StudentService,
              private _router:Router) { }

  ngOnInit() {
    this._addressService.get_address_by_year(this.bsValue.getFullYear()).subscribe((data)=>{
      this.address = data;
      this.list_address = data;
      console.log(this.address);
    },(error)=>{
      console.log(error);
    });

    this._historyService.get_history_students().subscribe((data)=>{
      this.list_year = data;
    },(error)=>{
      console.log(error);
    });

    this.year = this.bsValue.getFullYear();
  }

  // Delete Student
  // deleteStudent(student){
  //   if(confirm('คุณต้องการลบข้อมูลนี้หรือไม่')) {
  //     this._studentService.delete_student(student.studentCode).subscribe((data)=>{
  //       this.students.splice(this.students.indexOf(student),1);
  //       console.log("Delete Success");
  //     },(error)=>{
  //       console.log(error);
  //     });
  //   } else {
  //     console.log("Not delete Student");
  //   }

  // }

  // editStudent(student){
  //   this._studentService.setter(student);
  //   this._router.navigate(['/create_student']);
  // }

  // Go to Profile Page
  go_to_profile_page(studentCode:string){
    this._router.navigate(['/profile', studentCode]);
  }

  // Select year
  selectYear(value: number){
    // Get all Ability Detail
    this._addressService.get_address_by_year(value).subscribe((data)=>{
      this.address = data;
      this.list_address = data;
      console.log(this.address);
      console.log("List All Address Success");
    },(error)=>{
      console.log(error);
    });
  }

}
