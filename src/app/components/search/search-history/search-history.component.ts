import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HistoryStudentService } from 'app/services/history-student/history-student.service';
import { HistoryStudent } from 'app/entity/history-student';

@Component({
  selector: 'app-search-history',
  templateUrl: './search-history.component.html',
  styleUrls: ['./search-history.component.css']
})
export class SearchHistoryComponent implements OnInit {

  private historyStudent:HistoryStudent;
  private list_year:HistoryStudent;
  private list_history = [];

  private date = new Date();
  private year:number;

  private title:string = "ค้นหารายชื่อนักเรียน";
  private search_item = [
    {
      title: "ค้นหาโดยชั้นเรียน",
      placeholder: "ค้นหารายชื่อโดยชั้นเรียน"
    },
    {
      title: "ค้นหาโดยหอพัก",
      placeholder: "ค้นหารายชื่อโดยหอพัก"
    }
  ]

  constructor(private _router:Router,
              private _historyService:HistoryStudentService) { }

  ngOnInit() {
    this._historyService.get_by_year(this.date.getFullYear()).subscribe((data)=>{
      this.historyStudent = data;
      this.list_history = data;
      this.year = this.date.getFullYear()
    },(error)=>{
      console.log(error);
    });

    this._historyService.get_history_students().subscribe((data)=>{
      this.list_year = data;
    },(error)=>{
      console.log(error);
    });
  }

  // Go to Profile Page
  go_to_profile_page(studentCode:string){
    this._router.navigate(['/profile', studentCode]);
  }

  // Select year
  selectYear(value: number){
    // Get all Ability Detail
    this._historyService.get_by_year(value).subscribe((data)=>{
      this.historyStudent = data;
      this.list_history = data;
    },(error)=>{
      console.log(error);
    });
  }

}
