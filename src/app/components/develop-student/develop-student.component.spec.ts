import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevelopStudentComponent } from './develop-student.component';

describe('DevelopStudentComponent', () => {
  let component: DevelopStudentComponent;
  let fixture: ComponentFixture<DevelopStudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevelopStudentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevelopStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
