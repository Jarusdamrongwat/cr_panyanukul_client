import { Component, OnInit } from '@angular/core';
import { CurrentAddressService } from 'app/services/current-address/current-address.service';
import { ParentService } from 'app/services/parent/parent.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CreateAddressComponent } from 'app/components/create/create-address/create-address.component';
import { CurrentAddress } from 'app/entity/current-address';
import { Parent } from 'app/entity/parent';

@Component({
  selector: 'app-edit-parent',
  templateUrl: './edit-parent.component.html',
  styleUrls: ['./edit-parent.component.css']
})
export class EditParentComponent implements OnInit {

  public parent:Parent;
  private address_current:CurrentAddress;
  bsValue = new Date();

  public header_title:string = "เพิ่มข้อมูลนักเรียน";
  public address_student:string = "ที่อยู่ตามทะเบียนบ้าน";
  public current_address_student:string = "ที่อยู่ปัจจุบัน";
  public parent_:string = "ผู้ปกครอง";
  public address_parent:string = "ที่อยู่";

  private studentCode:string;

  list_prefix = ['นาย','นางสาว','นาง'];
  list_origin = ['ไทย','อาข่า','ม้ง','มูเซอ','เย้า','กะเหลี่ยง','ลาหู่','จีนฮ่อ','พม่า'];
  list_religion = ['พุทธ','คริส','อิสลาม','ฮินดู'];
  list_economic_status = ['มีฐานะดีมาก','มีฐานะดี','พอมีพอกิน','ยากจน'];

  list_amount = [];
  list_province = [];
  list_district = [];
  list_sub_district = [];

  constructor(private _addressCurrentService:CurrentAddressService,
    private _parentService:ParentService,
    private _routerActive:ActivatedRoute,
    private _addressComponent:CreateAddressComponent,
    private _router:Router) {
    for(let i = 0 ; i < 20 ; i++){
      this.list_amount.push(i+1);
    }
   }

  ngOnInit() {
    this.address_current = this._addressCurrentService.getter();
    this.parent = this._parentService.getter();

    this.list_province = this._addressComponent.list_province;
    this.list_district = this._addressComponent.list_districts;
    this.list_sub_district = this._addressComponent.list_sub_districts;

    this.studentCode = this._routerActive.snapshot.paramMap.get('id');
  }

  edit_information(){

    this._addressCurrentService.update_current_address(this.address_current).subscribe((data)=>{
      this.address_current = data;
      console.log(this.address_current);
    },(error)=>{
      console.log(error);
    });

    this._parentService.update_parent(this.parent,this.address_current.currentHomeId,this.studentCode).subscribe((data)=>{
      this.parent = data;
      console.log(this.parent);
      this._router.navigate(['/profile', this.studentCode]);
    },(error)=>{
      console.log(error);
    });
  }

}
