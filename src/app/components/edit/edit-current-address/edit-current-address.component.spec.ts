import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCurrentAddressComponent } from './edit-current-address.component';

describe('EditCurrentAddressComponent', () => {
  let component: EditCurrentAddressComponent;
  let fixture: ComponentFixture<EditCurrentAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCurrentAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCurrentAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
