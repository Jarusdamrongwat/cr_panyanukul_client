import { Component, OnInit } from '@angular/core';
import { Student } from 'app/entity/student';
import { HistoryStudent } from 'app/entity/history-student';
import { StudentService } from 'app/services/student/student.service';
import { HistoryStudentService } from 'app/services/history-student/history-student.service';
import { CrippleService } from 'app/services/cripple/cripple.service';
import { Cripple } from 'app/entity/cripple';
import { AddressHomeParticularService } from 'app/services/address-home-particular/address-home-particular.service';
import { CurrentAddressService } from 'app/services/current-address/current-address.service';
import { HealthService } from 'app/services/health/health.service';
import { FamilyMemberService } from 'app/services/family-member/family-member.service';
import { Health } from 'app/entity/health';
import { AddressHomeParticular } from 'app/entity/address-home-particular';
import { CurrentAddress } from 'app/entity/current-address';
import { FamilyMember } from 'app/entity/family-member';
import { CreateAddressComponent } from 'app/components/create/create-address/create-address.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-student',
  templateUrl: './edit-student.component.html',
  styleUrls: ['./edit-student.component.css']
})
export class EditStudentComponent implements OnInit {

  private student:Student
  private history_student:HistoryStudent;
  private health:Health;
  private address_home_particular:AddressHomeParticular;
  private address_current:CurrentAddress;
  private member:FamilyMember;

  private title_student:string = "ข้อมูลนักเรียน";
  private health_student:string = "ข้อมูลสุขภาพนักเรียน";
  private family_student:string = "ข้อมูลครอบครัว";
  private general_student:string = "ข้อมูลทั่วไป";
  public address_student:string = "ที่อยู่ตามทะเบียนบ้าน";
  public current_address_student:string = "ที่อยู่ปัจจุบัน";

  private list_province:any;
  private list_district:any;
  private list_sub_district:any;
  private cripples:Cripple;
  private crippleId:any;

  list_prefix = ['เด็กชาย','เด็กหญิง','นาย','นางสาว'];
  list_origin = ['ไทย','อาข่า','ม้ง','มูเซอ','เย้า','กะเหลี่ยง','ลาหู่','จีนฮ่อ','พม่า'];
  list_religion = ['พุทธ','คริส','อิสลาม','ฮินดู'];
  list_status_of_parent = ['อยู่ด้วยกัน','แยกกันอยู่','หย่าร้าง'];
  list_dormitory = ['หอชาย 1','หอชาย 2','หอชาย 3','หอชาย 4','หอชาย 5', 'หอชาย 6', 'หอหญิง 1','หอหญิง 2','หอหญิง 3','หอหญิง 4','หอหญิง 5'];
  list_classroom = [
    'อนุบาล 1','อนุบาล 2','อนุบาล 3', 'ประถมศึกษาปีที่ 1','ประถมศึกษาปีที่ 2','ประถมศึกษาปีที่ 3',
    'ประถมศึกษาปีที่ 4','ประถมศึกษาปีที่ 5','ประถมศึกษาปีที่ 6', 'มัธยมศึกษาปีที่ 1','มัธยมศึกษาปีที่ 2',
    'มัธยมศึกษาปีที่ 3','มัธยมศึกษาปีที่ 4','มัธยมศึกษาปีที่ 5','มัธยมศึกษาปีที่ 6'
  ];

  list_amount = [];
  progress_bar = 0;

  constructor(private _studentService:StudentService,
              private _historyService:HistoryStudentService,
              private _addressHomeService:AddressHomeParticularService,
              private _currentService:CurrentAddressService,
              private _addressComponent:CreateAddressComponent,
              private _familyService:FamilyMemberService,
              private _healthService:HealthService,
              private _crippleService:CrippleService,
              private _router:Router) {
                for(let i = 0 ; i < 20 ; i++){
                  this.list_amount.push(i+1);
                }

                this.progress_bar = 0;
              }

  ngOnInit() {
    this.student = this._studentService.getter();
    this.history_student = this._historyService.getter();
    this.health = this._healthService.getter();
    this.address_home_particular = this._addressHomeService.getter();
    this.address_current = this._currentService.getter();
    this.member = this._familyService.getter();
    this.crippleId = this.student.crippleId.crippleId;

    // Address List
    this.list_province = this._addressComponent.list_province;
    this.list_district = this._addressComponent.list_districts;
    this.list_sub_district = this._addressComponent.list_sub_districts;

    this._crippleService.get_cripples().subscribe((cripple)=>{
      this.cripples = cripple;
    },(error)=>{
      console.log(error);
    });
  }

  edit_information(){
    // Update Address Home Particular
    this._addressHomeService.update_address_home_particular(this.address_home_particular).subscribe((data)=>{
      this.address_home_particular = data;
      console.log("Update Address Home Success");
    },(error)=>{
      console.log(error);
    });

    // Update Current Address
    this._currentService.update_current_address(this.address_current).subscribe((data)=>{
      this.address_current = data;
      console.log("Update Current Address Success");
    },(error)=>{
      console.log(error);
    });

    // Update Family Member
    this._familyService.update_family_member(this.member).subscribe((data)=>{
      this.member = data;
      console.log("Update Family Member Success");
    },(error)=>{
      console.log(error);
    });

    // Update History Student
    this._historyService.update_history_student(this.history_student).subscribe((data)=>{
      this.history_student = data;
      console.log("Update History Student Success");
    },(error)=>{
      console.log(error);
    });
    // Update Student
    this._studentService.update_student(this.student,
                                        this.address_home_particular.homeParticularId,
                                        this.address_current.currentHomeId,
                                        this.crippleId,
                                        this.member.memberId)
    .subscribe((data)=>{
      this.student = data;
      console.log(this.student);
      this._router.navigate(['/profile', this.student.studentCode]);
    },(error)=>{
      console.log(error);
    });

  }

}
