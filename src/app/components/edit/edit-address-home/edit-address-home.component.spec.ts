import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAddressHomeComponent } from './edit-address-home.component';

describe('EditAddressHomeComponent', () => {
  let component: EditAddressHomeComponent;
  let fixture: ComponentFixture<EditAddressHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAddressHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAddressHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
