import { Component, OnInit, Input } from '@angular/core';
import { StudentService } from "../../services/student/student.service";
import { ParentService } from "../../services/parent/parent.service";
import { CurrentAddressService } from "../../services/current-address/current-address.service";
import { AddressHomeParticularService } from "../../services/address-home-particular/address-home-particular.service";
import { FamilyMemberService } from "../../services/family-member/family-member.service";
import { UploadImageService } from "../../services/upload-image/upload-image.service";

import { Student } from "../../entity/student";
import { Parent } from "../../entity/parent";
import { CurrentAddress } from "../../entity/current-address";
import { AddressHomeParticular } from "../../entity/address-home-particular";
import { FamilyMember } from "../../entity/family-member";
import { Router, ActivatedRoute } from "@angular/router";
import { AbilityHeaderService } from 'app/services/ability-header/ability-header.service';
import { AbilityService } from 'app/services/ability/ability.service';
import { AbilityDetailService } from 'app/services/ability-detail/ability-detail.service';
import { Ability } from 'app/entity/ability';
import { AbilityDetail } from 'app/entity/ability-detail';
import { AbilityHeader } from 'app/entity/ability-header';
import { ListAbility } from 'app/entity/list-ability';
import { ListAbilityService } from 'app/services/list-ability/list-ability.service';
import { Health } from 'app/entity/health';
import { HistoryStudent } from 'app/entity/history-student';
import { HealthService } from 'app/services/health/health.service';
import { HistoryStudentService } from 'app/services/history-student/history-student.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  private students:Student;
  private health:Health[];
  private historyStudent:HistoryStudent[];
  private parents:Parent[];
  private abilities:Ability[];
  private abilityDetail:AbilityDetail[];
  private abilityList:ListAbility[] = [];
  private year_ability:ListAbility[];
  private abilityScoreByHeaderId:ListAbility;
  private abilityHeaders:AbilityHeader[];
  private currentAddress:CurrentAddress;
  private currentAddressStudent:CurrentAddress;
  private addressHome:AddressHomeParticular;
  private members:FamilyMember;

  private studentCode:string;
  image_to_show:any;

  private title:string = "ข้อมูลนักเรียน";
  private personal_info:string = "ข้อมูลส่วนตัว";
  private general_info:string = "ข้อมูลทั่วไป";
  private address_info:string = "ที่อยู่ตามทะเบียนบ้าน";
  private current_address_info:string = "ที่อยู่ปัจจุบัน";
  private health_info:string = "ข้อมูลสุขภาพ";
  private parent_info:string = "ข้อมูลครอบครัว";

  private ability1:string = "ความสามารถตามหลักสูตร 1";
  private ability2:string = "ความสามารถตามหลักสูตร 2";
  private ability3:string = "ความสามารถตามหลักสูตร 3";
  private score_title:string = "ระดับความพอใจ";
  private imageUrl:string;
  private search_year:number;
  private year:number;
  private todayYear:number;
  private max_health:number;
  private max_history:number;
  private max_ability:number;

  list_score = [];
  today:number = Date.now();
  bsValue = new Date();

  // Find Score
  max:number = 0;
  totalScore:number = 0;
  arrayScore:Array<number> = [];
  showTotalScore:number = 0;

  constructor(private _studentService:StudentService,
              private _parentService:ParentService,
              private _currentService:CurrentAddressService,
              private _homeService:AddressHomeParticularService,
              private _memberService:FamilyMemberService,
              private _routerActive:ActivatedRoute,
              private _abilityHeaderService:AbilityHeaderService,
              private _abilityService:AbilityService,
              private _listAbilityService:ListAbilityService,
              private _abilityDetailService:AbilityDetailService,
              private _healthService:HealthService,
              private _historyStudentService:HistoryStudentService,
              private _router:Router) {
                for(let i = 0 ; i < 5 ; i++){
                  this.list_score.push(i+1);
                }
                this.studentCode = this._routerActive.snapshot.paramMap.get('id');
               }

  ngOnInit() {
    // Get Student By Student Code
    this._studentService.get_student_by_id(this.studentCode).subscribe((student)=>{
      this.students = student;
      this.todayYear = this.bsValue.getFullYear();
      // Load Image from Server
      this.imageUrl = "http://localhost:8080/images/displayImage?studentCode="+this.studentCode
      // Get Current Address by StudentCode
      this._currentService.get_address_by_studentCode(this.studentCode).subscribe((data)=>{
        this.currentAddressStudent = data;
      });
      // Get Address Particular by StudentCode
      this._homeService.get_address_by_studentCode(this.studentCode).subscribe((data)=>{
        this.addressHome = data;
      });
      this._memberService.get_family_member_by_studentCode(this.studentCode).subscribe((member)=>{
        this.members = member;
      });
      this._healthService.get_by_studentCode(this.studentCode).subscribe((data)=>{
        this.health = data;
        this.max_health = Math.max.apply(Math, this.health.map(function(item){
          return item.year;
        }));
      });
      this._historyStudentService.get_by_studentCode(this.studentCode).subscribe((data)=>{
        this.historyStudent = data;
        this.search_year = this.bsValue.getFullYear();
        this.max_history = Math.max.apply(Math, this.historyStudent.map(function(item){
          return item.year;
        }));
      })
    },(error)=>{
      console.log(error);
    });
    // Get Parents By Student Code
    this._parentService.get_parent_by_studentCode(this.studentCode).subscribe((parent)=>{
      this.parents = parent;
      for(let item of this.parents){
        this._currentService.get_address_by_parentId(item.parentId).subscribe((data)=>{
          this.currentAddress = data;
        })
      }
    },(error)=>{
      console.log(error);
    });

    // Get all Ability Headers
    this._abilityHeaderService.get_abilitys().subscribe((data)=>{
      this.abilityHeaders = data;
    },(error)=>{
      console.log(error);
    });

    // Get all Ability
    this._abilityService.get_abilitys().subscribe((data)=>{
      this.abilities = data;
      for(let item of this.abilities) {
        this._abilityDetailService.get_list_ability_by_abilityId(item.abilityId).subscribe((data)=>{
          this.abilityDetail = data;
          this.max = Math.max.apply(Math, this.abilityDetail.map(function(item){
            return item.abilityScore;
          }));
          this.totalScore += this.max;
        });
      }
    },(error)=>{
      console.log(error);
    });

    // Get all Ability Detail
    this.year = this.bsValue.getFullYear();
    this._listAbilityService.get_list_ability_by_studentCode(this.studentCode,this.year).subscribe((data)=>{
      this.abilityList = data;
      this.max_ability = Math.max.apply(Math, this.abilityList.map(function(item){
        return item.year;
      }));
    },(error)=>{
      console.log(error);
    });

    this._listAbilityService.get_list_ability_by_studentCode_only(this.studentCode).subscribe((data)=>{
      this.year_ability = data;
    },(error)=>{
      console.log(error);
    });
  }

  create_history_student(studentCode:string) {
    this._router.navigate(['/create_history_student', studentCode]);
  }

  create_health(studentCode:string) {
    this._router.navigate(['/create_health', studentCode]);
  }

  create_ability(studentCode:string) {
    this._router.navigate(['/create_ability_form_profile', studentCode]);
  }

  // Edit Student students,item,addressHome,currentAddress,members
  edit_student(student:Student, item:HistoryStudent, addressHome:AddressHomeParticular, currentAddress:CurrentAddress, members:FamilyMember) {
    this._studentService.setter(student);
    this._historyStudentService.setter(item);
    this._homeService.setter(addressHome);
    this._currentService.setter(currentAddress);
    this._memberService.setter(members);
    this._router.navigate(['/edit_student']);
  }

  // Edit Parent
  edit_parent(parent:Parent, currentAddress:CurrentAddress) {
    this._parentService.setter(parent);
    this._currentService.setter(currentAddress);
    this._router.navigate(['/edit_parent',this.studentCode]);
  }

  getSum():number {
    let sum = 0;
    for(let i = 0 ; i < this.abilityList.length ; i++) {
      sum += this.abilityList[i].abilityDetailId.abilityScore;
    }
    return sum;
  }

  getTotalScore(number:number):number {
    this.showTotalScore = number;
    return this.showTotalScore;
  }

  selectYear(value: number){
    // Get all Ability Detail
    this._listAbilityService.get_list_ability_by_studentCode(this.studentCode, value).subscribe((data)=>{
      this.abilityList = data;
    },(error)=>{
      console.log(error);
    });
  }
}
