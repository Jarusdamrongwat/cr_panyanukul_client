import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  private title:string;
  private location = [];
  constructor(private _router:Router) {
                this.title = "โรงเรียนเชียงรายปัญญานุกุล";
               }

  ngOnInit() {

  }

  // Go to Kindergarten Page
  go_to_kindergarten(){
    this._router.navigate(['/list_kindergarten']);
  }

  // Go to Primary Page
  go_to_primary_school(){
    this._router.navigate(['/list_primary_school']);
  }

  // Go to Secondary Page
  go_to_secondary_school(){
    this._router.navigate(['/list_secondary_school']);
  }

  // Test
  // update_classroom(classroom){
  //   this._classroomService.setter(classroom);
  //   this._router.navigate(['/create_student']);
  // }
  // create_classroom(){
  //   let classroom = new Classroom();
  //   this._classroomService.setter(classroom);
  //   this._router.navigate(['/create_student']);
  // }

}
