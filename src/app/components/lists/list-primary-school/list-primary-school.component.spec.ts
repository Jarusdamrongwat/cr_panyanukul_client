import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPrimarySchoolComponent } from './list-primary-school.component';

describe('ListPrimarySchoolComponent', () => {
  let component: ListPrimarySchoolComponent;
  let fixture: ComponentFixture<ListPrimarySchoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPrimarySchoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPrimarySchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
