import { Component, OnInit } from '@angular/core';
import { StudentService } from "../../../services/student/student.service";
import { Student } from "../../../entity/student";
import { Router } from "@angular/router";

@Component({
  selector: 'app-list-primary-school',
  templateUrl: './list-primary-school.component.html',
  styleUrls: ['./list-primary-school.component.css']
})
export class ListPrimarySchoolComponent implements OnInit {

  private students:Student[];
  private list_student = [];
  private class_name:string = "ประถมศึกษา";
  private header_title:string = "รายชื่อนักเรียนชั้นประถมศึกษาทั้งหมด";

  constructor(private _studentService:StudentService,
              private _router:Router) { }

  ngOnInit() {
    this._studentService.search_by_classname(this.class_name).subscribe((data)=>{
      this.students = data;
      this.list_student = data;
      console.log("List all Student");
    },(error)=>{
      console.log(error);
    })
  }

  // Delete Student
  deleteStudent(student){
    if(confirm('คุณต้องการลบข้อมูลนี้หรือไม่')) {
      this._studentService.delete_student(student.studentCode).subscribe((data)=>{
        this.students.splice(this.students.indexOf(student),1);
        console.log("Delete Success");
      },(error)=>{
        console.log(error);
      });
    } else {
      console.log("Not delete Student");
    }

  }

  editStudent(student){
    this._studentService.setter(student);
    this._router.navigate(['/create_student']);
  }

  // Go to Profile Page
  go_to_profile_page(studentCode:string){
    this._router.navigate(['/profile', studentCode]);
  }

}
