import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAbilityHeaderComponent } from './list-ability-header.component';

describe('ListAbilityHeaderComponent', () => {
  let component: ListAbilityHeaderComponent;
  let fixture: ComponentFixture<ListAbilityHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAbilityHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAbilityHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
