import { Component, OnInit } from '@angular/core';
import { AbilityHeaderService } from 'app/services/ability-header/ability-header.service';
import { AbilityService } from 'app/services/ability/ability.service';
import { Ability } from 'app/entity/ability';
import { AbilityHeader } from 'app/entity/ability-header';

@Component({
  selector: 'app-list-ability-header',
  templateUrl: './list-ability-header.component.html',
  styleUrls: ['./list-ability-header.component.css']
})
export class ListAbilityHeaderComponent implements OnInit {

  private abilities:Ability[];
  private abilityHeaders:AbilityHeader[];

  constructor(private _abilityHeaderService:AbilityHeaderService,
              private _abilityService:AbilityService) { }

  ngOnInit() {
    this._abilityHeaderService.get_abilitys().subscribe((data)=>{
      this.abilityHeaders = data;
      console.log(this.abilityHeaders)
    },(error)=>{
      console.log(error);
    })
  }

}
