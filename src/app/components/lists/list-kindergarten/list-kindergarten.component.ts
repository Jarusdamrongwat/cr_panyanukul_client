import { Component, OnInit, TemplateRef   } from '@angular/core';

import { StudentService } from '../../../services/student/student.service';
import { Student } from '../../../entity/student';
import { Router } from '@angular/router';
import { ParentService } from 'app/services/parent/parent.service';
import { Parent } from 'app/entity/parent';
import { AddressHomeParticularService } from 'app/services/address-home-particular/address-home-particular.service';
import { CurrentAddressService } from 'app/services/current-address/current-address.service';
import { FamilyMemberService } from 'app/services/family-member/family-member.service';
import { HistoryStudentService } from 'app/services/history-student/history-student.service';
import { HealthService } from 'app/services/health/health.service';
import { ListAbilityService } from 'app/services/list-ability/list-ability.service';
import { AddressHomeParticular } from 'app/entity/address-home-particular';
import { CurrentAddress } from 'app/entity/current-address';
import { FamilyMember } from 'app/entity/family-member';
import { HistoryStudent } from 'app/entity/history-student';
import { Health } from 'app/entity/health';
import { ListAbility } from 'app/entity/list-ability';

@Component({
  selector:  'app-list-kindergarten',
  templateUrl:  './list-kindergarten.component.html',
  styleUrls:  ['./list-kindergarten.component.css']
})
export class ListKindergartenComponent implements OnInit {

  private students:  Student[];
  private student:  Student;
  private parents: Parent;
  private addressHome: AddressHomeParticular;
  private currentAddress: CurrentAddress;
  private member: FamilyMember;
  private historyStudent: HistoryStudent;
  private health: Health;
  private listAbility: ListAbility;

  private list_student = [];
  private studentCode: string;

  private header_title: String = "รายชื่อนักเรียนชั้นอนุบาลทั้งหมด";
  private class_name: String = "อนุบาล";

  constructor(private _studentService:  StudentService,
              private _addressHomeService: AddressHomeParticularService,
              private _currentAddressService: CurrentAddressService,
              private _familyService: FamilyMemberService,
              private _parentService: ParentService,
              private _historyService: HistoryStudentService,
              private _healthService: HealthService,
              private _listAbilityService: ListAbilityService,
              private _router: Router) { }

  ngOnInit() {
    this._studentService.search_by_classname(this.class_name).subscribe((data) => {
      this.students = data;
      this.list_student = data;
      this.student = data;
      console.log(this.student);
      // Address Home Particular
      this._addressHomeService.get_address_by_studentCode(this.student.studentCode).subscribe((data) => {
        this.addressHome = data;
        console.log('Address Home = ' + this.addressHome);
      });
      // Current Address
      this._currentAddressService.get_address_by_studentCode(this.student.studentCode).subscribe((data) => {
        this.currentAddress = data;
        console.log('Current Address = ' + this.currentAddress);
      });
      // Family Member
      this._familyService.get_family_member_by_studentCode(this.student.studentCode).subscribe((data) => {
        this.member = data;
        console.log('Member = ' + this.member);
      });
      // Parent
      this._parentService.get_parent_by_studentCode(this.student.studentCode).subscribe((data) => {
        this.parents = data;
        console.log('Parent = ' + this.parents);
      });
      // History Student
      this._historyService.get_by_studentCode(this.student.studentCode).subscribe((data) => {
        this.historyStudent = data;
        console.log('History = ' + this.historyStudent);
      });
      // Health
      this._healthService.get_by_studentCode(this.student.studentCode).subscribe((data) => {
        this.health = data;
        console.log(this.health);
      });
      // List Ability
      this._listAbilityService.get_list_ability_by_studentCode_only(this.student.studentCode).subscribe((data) => {
        this.listAbility = data;
        console.log(this.listAbility);
      });
      console.log('List all Student');
    }, (error) => {
      console.log(error);
    });
  }

  // Delete Student
  deleteStudent(student){
    if(confirm('คุณต้องการลบข้อมูลนี้หรือไม่')) {
      // Delete Address Home
      // this._addressHomeService.delete_address_home_particular().subscribe((data) => {
      //   console.log('Delete Address Home Success');
        // Delete Student
        this._studentService.delete_student(student.studentCode).subscribe((data) => {
          this.students.splice(this.students.indexOf(student), 1 );
          console.log('Delete Success');
        },(error) => {
          console.log(error);
        });
      // },(error) => {
      //   console.log(error);
      // });
    } else {
      console.log('Not delete Student');
    }

  }

  editStudent(student){
    this._studentService.setter(student);
    this._router.navigate(['/create_student']);
  }

  // Go to Profile Page
  go_to_profile_page(studentCode:  string){
    this._router.navigate(['/profile', studentCode]);
  }

}
