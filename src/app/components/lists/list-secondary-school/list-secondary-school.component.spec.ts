import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSecondarySchoolComponent } from './list-secondary-school.component';

describe('ListSecondarySchoolComponent', () => {
  let component: ListSecondarySchoolComponent;
  let fixture: ComponentFixture<ListSecondarySchoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSecondarySchoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSecondarySchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
