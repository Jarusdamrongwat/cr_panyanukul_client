import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private username:String = "";
  private password:String = "";
  constructor(private _router:Router) { }

  ngOnInit() {
  }

  login(username,password){
    if(this.username == username && this.password == password){
        this._router.navigate(['/'])
    }
  }

}
