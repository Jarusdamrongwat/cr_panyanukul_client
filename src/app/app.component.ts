import { Component, OnInit } from '@angular/core';
import { HeaderComponent } from "./components/header/header.component";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'app works!';

  constructor(private _headerComponent:HeaderComponent){ }

  ngOnInit(){
    this._headerComponent.go_to_search_page();
    this._headerComponent.go_to_create_page();
    this._headerComponent.go_to_index_page();
  }
}
