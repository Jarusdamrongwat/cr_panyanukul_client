import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(students: Array<any>, search_fname:string,
                                  search_lname:string,
                                  search_congenitalDisease:string,
                                  search_cripple:string): any {
    // Check If search is Undefined
    if(students && students.length){
      return students.filter(function(student){
        if(search_fname && student.firstName.toLowerCase().indexOf(search_fname.toLowerCase()) === -1){
          return false;
        }
        if(search_lname && student.lastName.toLowerCase().indexOf(search_lname.toLowerCase()) === -1){
          return false;
        }
        if(search_congenitalDisease && student.congenitalDisease.toLowerCase().indexOf(search_congenitalDisease.toLowerCase()) === -1){
          return false;
        }
        if(search_cripple && student.crippleId.crippleType.toLowerCase().indexOf(search_cripple.toLowerCase()) === -1){
          return false;
        }
        return true;
      })
    } else {
      return students;
    }
  }
}
