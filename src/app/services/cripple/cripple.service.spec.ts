import { TestBed, inject } from '@angular/core/testing';

import { CrippleService } from './cripple.service';

describe('CrippleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CrippleService]
    });
  });

  it('should be created', inject([CrippleService], (service: CrippleService) => {
    expect(service).toBeTruthy();
  }));
});
