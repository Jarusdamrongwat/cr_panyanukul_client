import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { Cripple } from "../../entity/cripple";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class CrippleService {

  private baseUrl:string = "http://localhost:8080/cripple";
  private headers = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  private cripple = new Cripple();

  constructor(private _http:Http) { }

  get_cripples(){
    return this._http.get(this.baseUrl+'/list',this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_cripple_by_id(id:number){
    return this._http.get(this.baseUrl+'/list/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  delete_cripple(id:number){
    return this._http.delete(this.baseUrl+'/delete/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  create_cripple(cripple:Cripple){
    return this._http.post(this.baseUrl+'/add', JSON.stringify(cripple) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  update_cripple(cripple:Cripple){
    return this._http.put(this.baseUrl+'/update', JSON.stringify(cripple) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  errorHandler(error:Response){
    return Observable.throw(error || "SERVER ERROR")
  }
  setter(cripple:Cripple){
    this.cripple = cripple;
  }
  getter(){
    return this.cripple;
  }

}
