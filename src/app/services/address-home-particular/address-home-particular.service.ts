import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { AddressHomeParticular } from "../../entity/address-home-particular";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AddressHomeParticularService {

  private baseUrl:string = "http://localhost:8080/address_home_particular";
  private headers = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  private address_home_particular = new AddressHomeParticular();

  constructor(private _http:Http) { }

  get_address_home_particulars(){
    return this._http.get(this.baseUrl+'/list',this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_address_home_particular_by_id(id:number){
    return this._http.get(this.baseUrl+'/list/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_address_by_studentCode(studentCode:string){
    return this._http.get(this.baseUrl+'/find_studentCode/'+studentCode,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_address_by_year(year:number){
    return this._http.get(this.baseUrl+'/search_year/'+year,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  delete_address_home_particular(id:number){
    return this._http.delete(this.baseUrl+'/delete/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  create_address_home_particular(address_home_particular:AddressHomeParticular){
    return this._http.post(this.baseUrl+'/add', JSON.stringify(address_home_particular) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  update_address_home_particular(address_home_particular:AddressHomeParticular){
    return this._http.put(this.baseUrl+'/update', JSON.stringify(address_home_particular) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  errorHandler(error:Response){
    return Observable.throw(error || "SERVER ERROR")
  }
  setter(address_home_particular:AddressHomeParticular){
    this.address_home_particular = address_home_particular;
  }
  getter(){
    return this.address_home_particular;
  }

}
