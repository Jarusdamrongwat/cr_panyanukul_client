import { TestBed, inject } from '@angular/core/testing';

import { AddressHomeParticularService } from './address-home-particular.service';

describe('AddressHomeParticularService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddressHomeParticularService]
    });
  });

  it('should be created', inject([AddressHomeParticularService], (service: AddressHomeParticularService) => {
    expect(service).toBeTruthy();
  }));
});
