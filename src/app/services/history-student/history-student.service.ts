import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { HistoryStudent } from "../../entity/history-student";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class HistoryStudentService {

  private baseUrl:string = "http://localhost:8080/history_student";
  private headers = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  private history_student = new HistoryStudent();

  constructor(private _http:Http) { }

  get_history_students(){
    return this._http.get(this.baseUrl+'/list',this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_history_student_by_id(id:number){
    return this._http.get(this.baseUrl+'/list/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_by_studentCode(studentCode:string){
    return this._http.get(this.baseUrl+'/find_studentCode/'+studentCode,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_by_year(year:number){
    return this._http.get(this.baseUrl+'/search_year/'+year,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  delete_history_student(id:number){
    return this._http.delete(this.baseUrl+'/delete/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  create_history_student(history_student:HistoryStudent, studentCode:string){
    return this._http.post(this.baseUrl+'/add/'+studentCode, JSON.stringify(history_student) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  update_history_student(history_student:HistoryStudent){
    return this._http.put(this.baseUrl+'/update', JSON.stringify(history_student) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  errorHandler(error:Response){
    return Observable.throw(error || "SERVER ERROR")
  }
  setter(history_student:HistoryStudent){
    this.history_student = history_student;
  }
  getter(){
    return this.history_student;
  }

}
