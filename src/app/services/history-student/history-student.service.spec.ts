import { TestBed, inject } from '@angular/core/testing';

import { HistoryStudentService } from './history-student.service';

describe('HistoryStudentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HistoryStudentService]
    });
  });

  it('should be created', inject([HistoryStudentService], (service: HistoryStudentService) => {
    expect(service).toBeTruthy();
  }));
});
