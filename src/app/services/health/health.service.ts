import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { Health } from "../../entity/health";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class HealthService {

  private baseUrl:string = "http://localhost:8080/health";
  private headers = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  private health = new Health();

  constructor(private _http:Http) { }

  get_healths(){
    return this._http.get(this.baseUrl+'/list',this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_health_by_id(id:number){
    return this._http.get(this.baseUrl+'/list/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_by_studentCode(studentCode:string){
    return this._http.get(this.baseUrl+'/find_studentCode/'+studentCode,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  delete_health(id:number){
    return this._http.delete(this.baseUrl+'/delete/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  create_health(health:Health, studentCode:string){
    return this._http.post(this.baseUrl+'/add/'+studentCode, JSON.stringify(health) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  update_health(health:Health){
    return this._http.put(this.baseUrl+'/update', JSON.stringify(health) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  errorHandler(error:Response){
    return Observable.throw(error || "SERVER ERROR")
  }
  setter(health:Health){
    this.health = health;
  }
  getter(){
    return this.health;
  }

}
