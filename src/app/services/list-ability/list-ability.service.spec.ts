import { TestBed, inject } from '@angular/core/testing';

import { ListAbilityService } from './list-ability.service';

describe('ListAbilityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListAbilityService]
    });
  });

  it('should be created', inject([ListAbilityService], (service: ListAbilityService) => {
    expect(service).toBeTruthy();
  }));
});
