import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { ListAbility } from "../../entity/list-ability";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class ListAbilityService {

  private baseUrl:string = "http://localhost:8080/list_ability";
  private headers = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  private ability = new ListAbility();

  constructor(private _http:Http) { }

  get_list_ability(){
    return this._http.get(this.baseUrl+'/list',this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_ability_by_id(id:number){
    return this._http.get(this.baseUrl+'/list/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_list_ability_by_headerId(headerId:number){
    return this._http.get(this.baseUrl+'/find_abilityHeaderId/'+headerId,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_list_ability_by_studentCode(studentCode:string, year:number){
    return this._http.get(this.baseUrl+'/find_studentCode/'+studentCode+'/'+year,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_list_ability_by_studentCode_only(studentCode:string){
    return this._http.get(this.baseUrl+'/find_studentCode/'+studentCode,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  delete_ability(id:number){
    return this._http.delete(this.baseUrl+'/delete/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  create_ability(ability:ListAbility, abilityId:number, studentCode:string){
    return this._http.post(this.baseUrl+'/add/'+abilityId+'/'+studentCode, JSON.stringify(ability) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  update_ability(ability:ListAbility[]){
    return this._http.put(this.baseUrl+'/update', JSON.stringify(ability) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  errorHandler(error:Response){
    return Observable.throw(error || "SERVER ERROR")
  }
  setter(ability:ListAbility){
    this.ability = ability;
  }
  getter(){
    return this.ability;
  }

}
