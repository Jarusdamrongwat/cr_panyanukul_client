import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { FamilyMember } from "../../entity/family-member";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class FamilyMemberService {

  private baseUrl:string = "http://localhost:8080/family_member";
  private headers = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  private family_member = new FamilyMember();

  constructor(private _http:Http) { }

  get_family_members(){
    return this._http.get(this.baseUrl+'/list',this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_family_member_by_id(id:number){
    return this._http.get(this.baseUrl+'/list/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_family_member_by_studentCode(studentCode:string){
    return this._http.get(this.baseUrl+'/find_studentCode/'+studentCode,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  delete_family_member(id:number){
    return this._http.delete(this.baseUrl+'/delete/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  create_family_member(family_member:FamilyMember){
    return this._http.post(this.baseUrl+'/add', JSON.stringify(family_member) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  update_family_member(family_member:FamilyMember){
    return this._http.put(this.baseUrl+'/update', JSON.stringify(family_member) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  errorHandler(error:Response){
    return Observable.throw(error || "SERVER ERROR")
  }
  setter(family_member:FamilyMember){
    this.family_member = family_member;
  }
  getter(){
    return this.family_member;
  }

}
