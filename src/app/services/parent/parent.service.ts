import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { Parent } from "../../entity/parent";
import { CurrentAddress } from '../../entity/current-address';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { CurrentAddressService } from '../current-address/current-address.service';

@Injectable()
export class ParentService {

  private baseUrl:string = "http://localhost:8080/parent";
  private baseUrl_current:string = "http://localhost:8080/current_address";
  private headers = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  private parent = new Parent();
  private currentAddress = new CurrentAddress();

  constructor(private _http:Http, private _currentService:CurrentAddressService) { }

  get_parents(){
    return this._http.get(this.baseUrl+'/list',this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_current_addresss(){
    return this._http.get(this.baseUrl_current+'/list',this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  public get_current_by_id(id:number){
    return this.get_current_addresss().map(current =>{
      return current.currentHomeId === id;
    })
  }
  get_parent_by_id(id:number){
    return this._http.get(this.baseUrl+'/list/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_parent_by_studentCode(studentCode:string){
    return this._http.get(this.baseUrl+'/find_studentCode/'+studentCode,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_parent_by_current(currentId:number){
    return this._http.get(this.baseUrl+'/find_current/'+currentId,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  delete_parent(id:number){
    return this._http.delete(this.baseUrl+'/delete/'+id, this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  create_parent(parent:Parent, currentId:number, studentCode:string){
    return this._http.post(this.baseUrl+'/add/'+currentId+'/'+studentCode, JSON.stringify(parent) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  update_parent(parent:Parent, currentId:number, studentCode:string){
    return this._http.put(this.baseUrl+'/update/'+currentId+'/'+studentCode, JSON.stringify(parent) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  errorHandler(error:Response){
    return Observable.throw(error || "SERVER ERROR")
  }
  setter(parent:Parent){
    this.parent = parent;
  }
  getCurrent(){
    return this.parent.getCurrentAddress();
  }
  getter(){
    return this.parent;
  }

}
