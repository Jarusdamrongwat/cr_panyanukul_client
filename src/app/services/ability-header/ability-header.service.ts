import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { AbilityHeader } from 'app/entity/ability-header';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AbilityHeaderService {

  private baseUrl:string = "http://localhost:8080/ability_header";
  private headers = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  private ability = new AbilityHeader();

  constructor(private _http:Http) { }

  get_abilitys(){
    return this._http.get(this.baseUrl+'/list',this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_ability_by_id(id:number){
    return this._http.get(this.baseUrl+'/list/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  delete_ability(id:number){
    return this._http.delete(this.baseUrl+'/delete/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  create_ability(ability:AbilityHeader){
    return this._http.post(this.baseUrl+'/add', JSON.stringify(ability) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  update_ability(ability:AbilityHeader){
    return this._http.put(this.baseUrl+'/update', JSON.stringify(ability) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  errorHandler(error:Response){
    return Observable.throw(error || "SERVER ERROR")
  }
  setter(ability:AbilityHeader){
    this.ability = ability;
  }
  getter(){
    return this.ability;
  }

}
