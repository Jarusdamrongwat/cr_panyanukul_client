import { TestBed, inject } from '@angular/core/testing';

import { AbilityHeaderService } from './ability-header.service';

describe('AbilityHeaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AbilityHeaderService]
    });
  });

  it('should be created', inject([AbilityHeaderService], (service: AbilityHeaderService) => {
    expect(service).toBeTruthy();
  }));
});
