import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Student } from '../../entity/student';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class StudentService {

  private baseUrl: String = 'http: //localhost: 8080/student';
  private headers = new Headers();
  private options = new RequestOptions({headers: this.headers});
  private student = new Student();
  private response: Response;

  constructor(private _http: Http) {
    this.headers.set('Content-Type', 'application/json; charset=UTF-8');
   }

  // Get All Students
  get_students() {
    return this._http.get(this.baseUrl + '/list', this.options).map((response: Response)  =>  response.json())
      .catch(this.errorHandler);
  }

  // Get Student By Student Code
  get_student_by_id(id: String) {
    return this._http.get(this.baseUrl + '/list/' + id, this.options).map((response: Response)  =>  response.json())
      .catch(this.errorHandler);
  }

  get_by_year(year: number) {
    return this._http.get(this.baseUrl + '/search_year/' + year,this.options).map((response: Response)  =>  response.json())
      .catch(this.errorHandler);
  }

  // Search Student
    // Search Student by Class Name
    search_by_classname(class_name: String) {
      return this._http.get(this.baseUrl + '/list_by_classname/' + class_name, this.options).map((response: Response)  =>  response.json())
      .catch(this.errorHandler);
    }

    // Search Student by Province
    search_by_province(province: String) {
      return this._http.get(this.baseUrl + '/list_by_province/' + province, this.options).map((response: Response)  =>  response.json())
      .catch(this.errorHandler);
    }

    // Search Student by District
    search_by_district(district: String) {
      return this._http.get(this.baseUrl + '/list_by_district/' + district, this.options).map((response: Response)  =>  response.json())
      .catch(this.errorHandler);
    }

    // Search Student by Sub District
    search_by_sub_district(sub_district: String) {
      return this._http.get(this.baseUrl + '/list_by_sub_district/' + sub_district, this.options)
        .map((response: Response)  =>  response.json())
        .catch(this.errorHandler);
    }

    // Search Student by Sub District
    search_by_firstname_or_lastname(firstname: String, lastname: String) {
      return this._http.get(this.baseUrl + '/list_by_fname_or_lname/' + firstname + '/' + lastname, this.options)
        .map((response: Response)  =>  response.json())
        .catch(this.errorHandler);
    }

  // Delete Student
  delete_student(id: String) {
    return this._http.delete(this.baseUrl + '/delete/' + id,this.options).map((response: Response) => response.status)
      .catch(this.errorHandler);
  }

  // Create Student
  create_student_with_image(student: Student, addressId: number, currentId: number, crippleId: number, memberId: number, files: File) {
    return this._http.post(this.baseUrl + '/add/' + addressId + '/' + currentId + '/' + crippleId + '/' + memberId + '?file=' + files,
      JSON.stringify(student) , this.options)
      .map((response: Response)  =>  response.json())
      .catch(this.errorHandler);
  }
  // Create Student
  create_student(student: Student, addressId: number, currentId: number, crippleId: number, memberId: number) {
    return this._http.post(this.baseUrl + '/add/' + addressId + '/' + currentId + '/' + crippleId + '/' + memberId,
      JSON.stringify(student) ,this.options)
      .map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  // Update Student
  update_student(student: Student, addressId: number, currentId: number, crippleId: number, memberId: number) {
    return this._http.put(this.baseUrl + '/update/' + addressId + '/' + currentId + '/' + crippleId + '/' + memberId,
      JSON.stringify(student) ,this.options)
      .map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  // Error Handler
  errorHandler(error: Response) {
    return Observable.throw(error || 'SERVER ERROR');
  }

  // Setter
  setter(student: Student) {
    this.student = student;
  }

  // Getter
  getter() {
    return this.student;
  }

}
