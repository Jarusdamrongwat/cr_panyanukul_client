import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { AbilityDetail } from "../../entity/ability-detail";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AbilityDetailService {

  private baseUrl:string = "http://localhost:8080/ability_detail";
  private headers = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  private ability = new AbilityDetail();

  constructor(private _http:Http) { }

  get_abilitys(){
    return this._http.get(this.baseUrl+'/list',this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }

  get_ability_by_id(id:number){
    return this._http.get(this.baseUrl+'/list/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_list_ability_by_abilityId(abilityId:number){
    return this._http.get(this.baseUrl+'/find_abilityId/'+abilityId,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  delete_ability(id:number){
    return this._http.delete(this.baseUrl+'/delete/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  create_ability(ability:AbilityDetail, abilityId:number){
    return this._http.post(this.baseUrl+'/add/'+abilityId, JSON.stringify(ability) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  update_ability(ability:AbilityDetail){
    return this._http.put(this.baseUrl+'/update', JSON.stringify(ability) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  errorHandler(error:Response){
    return Observable.throw(error || "SERVER ERROR")
  }
  setter(ability:AbilityDetail){
    this.ability = ability;
  }
  getter(){
    return this.ability;
  }

}
