import { TestBed, inject } from '@angular/core/testing';

import { AbilityDetailService } from './ability-detail.service';

describe('AbilityDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AbilityDetailService]
    });
  });

  it('should be created', inject([AbilityDetailService], (service: AbilityDetailService) => {
    expect(service).toBeTruthy();
  }));
});
