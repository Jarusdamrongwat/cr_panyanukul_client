import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { CurrentAddress } from "../../entity/current-address";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class CurrentAddressService {

  private baseUrl:string = "http://localhost:8080/current_address";
  private headers = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  private current_address = new CurrentAddress();

  constructor(private _http:Http) { }

  get_current_addresss(){
    return this._http.get(this.baseUrl+'/list',this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_current_address_by_id(id:number){
    return this._http.get(this.baseUrl+'/list/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_address_by_parentId(parentId:number){
    return this._http.get(this.baseUrl+'/find_parent/'+parentId,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  get_address_by_studentCode(studentCode:string){
    return this._http.get(this.baseUrl+'/find_studentCode/'+studentCode,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  delete_current_address(id:number){
    return this._http.delete(this.baseUrl+'/delete/'+id,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  create_current_address(current_address:CurrentAddress){
    return this._http.post(this.baseUrl+'/add', JSON.stringify(current_address) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  update_current_address(current_address:CurrentAddress){
    return this._http.put(this.baseUrl+'/update', JSON.stringify(current_address) ,this.options).map((response:Response)=>response.json())
      .catch(this.errorHandler);
  }
  errorHandler(error:Response){
    return Observable.throw(error || "SERVER ERROR")
  }
  setter(current_address:CurrentAddress){
    this.current_address = current_address;
  }
  getter(){
    return this.current_address;
  }

}
