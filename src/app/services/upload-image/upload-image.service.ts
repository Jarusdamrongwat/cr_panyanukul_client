import {Injectable} from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { Http, Response, Headers, RequestOptions, ResponseContentType, ResponseType } from "@angular/http";
import { Observable } from "rxjs/Observable";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class UploadImageService {

  private baseUrl:string = "http://localhost:8080/images";

  constructor(private _http:HttpClient) { }

  save_image(formData: FormData){
    return this._http.post(this.baseUrl+'/upload', formData, {responseType: 'text'})
      .catch(this.errorHandler);
  }

  // Error Handler
  errorHandler(error:Response){
    return Observable.throw(error || "SERVER ERROR")
  }

}
