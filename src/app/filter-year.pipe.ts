import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterYear'
})
export class FilterYearPipe implements PipeTransform {

  transform(historyStudent: any[], search_year: string): any {
    // return typeof search_year !== 'undefined'
    // ? historyStudent.filter(search => search.year === search_year)
    // : historyStudent;
    // Check If search is Undefined
    if(historyStudent && historyStudent.length){
      return historyStudent.filter(function(student){
        if(search_year && student.year.toString().indexOf(search_year.toString()) === -1){
          return false;
        }
        return true;
      })
    } else {
      return historyStudent;
    }
  }

}
