import { AbilityDetail } from "./ability-detail";
import { Student } from "./student";

export class ListAbility {
  listAbilityId:number;
  studentCode:Student;
  year:number;

  abilityDetailId:AbilityDetail;
}
