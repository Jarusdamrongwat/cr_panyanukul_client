import { Parent } from "./parent";

export class CurrentAddress {
  currentHomeId:number;
  houseNumber:string;
  village:string;
  soi:string;
  road:string;
  district:string;
  subDistrict:string;
  province:string;
  postCode:number;
  parentId:Parent;
}
