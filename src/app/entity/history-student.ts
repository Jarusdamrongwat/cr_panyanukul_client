import { Student } from "./student";

export class HistoryStudent {
  id:number;
  habit:string;
  classroom:string;
  teacherName:string;
  dormitoryName:string;
  year:number;
  studentCode:Student;
}
