import { AddressHomeParticular } from "./address-home-particular";
import { CurrentAddress } from "./current-address";
import { FamilyMember } from "./family-member";
import { Cripple } from "./cripple";

export class Student {
  studentCode:string;
  image:string;
  prefix:string;
  firstName:string;
  lastName:string;
  birthDate:Date;
  idCard:string;
  origin:string;
  nationality:string;
  religion:string;
  previousSchool:string;
  congenitalDisease:string;
  personalMedicine:string;
  allergy:string;
  crippleCard:string
  year:number;
  // habit:string;
  complexCripple:string;
  currentClassroom:string;
  startedClassroom:string;
  // dormitoryName:string;

  crippleId:Cripple;
  setCripple(crippleId:Cripple){
    this.crippleId = crippleId;
  }
  getCripple(){
    return this.crippleId;
  }
  memberId:FamilyMember;
  setFamilyMember(memberId:FamilyMember){
    this.memberId = memberId;
  }
  getFamilyMember(){
    return this.memberId;
  }
  currentHomeId:CurrentAddress;
  setCurrentAddress(currentHomeId:CurrentAddress){
    this.currentHomeId = currentHomeId;
  }
  getCurrentAddress(){
    return this.currentHomeId;
  }
  homeParticularId:AddressHomeParticular;
  setAddressHomeParticular(homeParticularId:AddressHomeParticular){
    this.homeParticularId = homeParticularId;
  }
  getAddressHomeParticular(){
    return this.homeParticularId;
  }
}
