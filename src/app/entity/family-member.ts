export class FamilyMember {
  memberId:number;
  amountMembers:number;
  memberDetail:string;
  relationOfFamily:string;
  statusOfParent:string;
  amountSiblings:number;
  siblingsDetail:string;
  yourOrderInFamily:string;
}
