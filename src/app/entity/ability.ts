import { AbilityHeader } from "./ability-header";

export class Ability {
  abilityId:number;
  abilitySubHeader:string;

  abilityHeaderId:AbilityHeader;
}
