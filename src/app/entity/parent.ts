import { CurrentAddress } from "../entity/current-address";
export class Parent {
  parentId:number;
  type:string;
  prefix:string;
  firstName:string;
  lastName:string;
  birthDate:Date;
  origin:string;
  nationality:string;
  religion:string;
  job:string;
  economicStatus:string;
  relationship:string;
  isGuardian:boolean;
  telephone:number;
  year:number;

  studentCode:string;

  currentHomeId:CurrentAddress;
  setCurrentAddress(currentHomeId:CurrentAddress){
    this.currentHomeId = currentHomeId;
  }
  getCurrentAddress(){
    return this.currentHomeId;
  }
}
