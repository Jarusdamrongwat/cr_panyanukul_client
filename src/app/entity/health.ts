import { Student } from "./student";

export class Health {
  healthId:number;
  weight:number;
  height:number;
  historyHealth:string;
  year:number;
  studentCode:Student;
}
