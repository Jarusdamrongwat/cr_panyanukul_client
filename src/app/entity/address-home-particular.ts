export class AddressHomeParticular {
  homeParticularId:number;
  houseNumber:string;
  village:string;
  soi:string;
  road:string;
  district:string;
  subDistrict:string;
  province:string;
  postCode:number;
}
