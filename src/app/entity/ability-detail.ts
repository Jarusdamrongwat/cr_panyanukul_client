import { Ability } from "./ability";

export class AbilityDetail {
  abilityDetailId:number;
  abilityDetail:string;
  abilityScore:number;

  abilityId:Ability;
}
