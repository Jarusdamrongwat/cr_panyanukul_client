import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from "@angular/router";
import { HttpClientModule } from '@angular/common/http';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { AccordionModule } from 'ngx-bootstrap/accordion';

import { ConfirmationPopoverModule } from 'angular-confirmation-popover';

import { AppComponent } from './app.component';
import { IndexComponent } from './components/index/index.component';
import { CreateStudentComponent } from './components/create/create-student/create-student.component';
import { LoginComponent } from './components/login/login.component';
import { SearchInformationComponent } from './components/search/search-information/search-information.component';
import { ListKindergartenComponent } from './components/lists/list-kindergarten/list-kindergarten.component';
import { ListPrimarySchoolComponent } from './components/lists/list-primary-school/list-primary-school.component';
import { ListSecondarySchoolComponent } from './components/lists/list-secondary-school/list-secondary-school.component';
import { HeaderComponent } from './components/header/header.component';
import { ProfileComponent } from './components/profile/profile.component';
import { CreateParentComponent } from './components/create/create-parent/create-parent.component';
import { CreateAddressComponent } from './components/create/create-address/create-address.component';

import { AbilityService } from "./services/ability/ability.service";
import { AddressHomeParticularService } from "./services/address-home-particular/address-home-particular.service";
import { CrippleService } from "./services/cripple/cripple.service";
import { CurrentAddressService } from "./services/current-address/current-address.service";
import { FamilyMemberService } from "./services/family-member/family-member.service";
import { ParentService } from "./services/parent/parent.service";
import { StudentService } from "./services/student/student.service";
import { ListAbilityService } from "./services/list-ability/list-ability.service";
import { CreateMotherComponent } from './components/create/create-mother/create-mother.component';
import { CreateFatherComponent } from './components/create/create-father/create-father.component';
import { UploadImageService } from './services/upload-image/upload-image.service';

import 'rxjs/add/observable/throw';
import { FilterPipe } from './filter.pipe';
import { CreateAbilityComponent } from './components/create/create-ability/create-ability.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { GroupByPipe } from './group-by.pipe';
import { AbilityHeaderService } from './services/ability-header/ability-header.service';
import { ListAbilityHeaderComponent } from './components/lists/list-ability-header/list-ability-header/list-ability-header.component';
import { CreateAbilityHeaderComponent } from './components/create/create-ability-header/create-ability-header/create-ability-header.component';
import { AbilityFilterPipe } from './filter/ability-filter.pipe';
import { AbilityHeaderFilterPipe } from './filter/ability-header-filter.pipe';
import { CreateAbilityDetailComponent } from './components/create/create-ability-detail/create-ability-detail.component';
import { AbilityDetailService } from './services/ability-detail/ability-detail.service';
import { CreateAbilityFormComponent } from './components/create/create-ability-form/create-ability-form.component';
import { AbilityListfilterPipe } from './filter/ability-listfilter.pipe';
import { AbilityListHeaderfilterPipe } from './filter/ability-list-headerfilter.pipe';
import { SearchAddressComponent } from './components/search/search-address/search-address.component';
import { FilterAddressPipe } from './filter-address.pipe';
import { FilterYearPipe } from './filter-year.pipe';
import { DevelopStudentComponent } from './components/develop-student/develop-student.component';
import { DevelopStudentFormComponent } from './components/create/develop-student-form/develop-student-form.component';
import { HealthService } from './services/health/health.service';
import { HistoryStudentService } from './services/history-student/history-student.service';
import { CreateHealthComponent } from './components/create/create-health/create-health.component';
import { CreateHistoryStudentComponent } from './components/create/create-history-student/create-history-student.component';
import { SearchHistoryComponent } from './components/search/search-history/search-history.component';
import { SearchHistoryPipe } from './filter/search-history.pipe';
import { FilterYearAbilityPipe } from './filter/filter-year-ability.pipe';
import { AbilityFormProfileComponent } from './components/create/ability-form-profile/ability-form-profile.component';
import { EditStudentComponent } from './components/edit/edit-student/edit-student.component';
import { EditAddressHomeComponent } from './components/edit/edit-address-home/edit-address-home.component';
import { EditCurrentAddressComponent } from './components/edit/edit-current-address/edit-current-address.component';
import { EditHealthComponent } from './components/edit/edit-health/edit-health.component';
import { EditParentComponent } from './components/edit/edit-parent/edit-parent.component';

const appRoutes: Routes = [
  { path: '', component: IndexComponent, pathMatch: 'full'},
  { path: 'list_kindergarten', component: ListKindergartenComponent },
  { path: 'list_primary_school', component: ListPrimarySchoolComponent },
  { path: 'list_secondary_school', component: ListSecondarySchoolComponent },
  { path: 'profile/:id', component: ProfileComponent },
  { path: 'create_student', component: CreateStudentComponent },
  { path: 'create_health/:id', component: CreateHealthComponent },
  { path: 'create_history_student/:id', component: CreateHistoryStudentComponent },
  { path: 'create_ability_form/:id', component: CreateAbilityFormComponent },
  { path: 'create_ability_form_profile/:id', component: AbilityFormProfileComponent },
  { path: 'create_develop_form/:id', component: DevelopStudentFormComponent },
  { path: 'create_parent/:id', component: CreateParentComponent },
  { path: 'create_mother/:id', component: CreateMotherComponent },
  { path: 'create_father/:id', component: CreateFatherComponent },
  { path: 'create_ability', component: CreateAbilityComponent },
  { path: 'create_ability_detail', component: CreateAbilityDetailComponent },
  { path: 'create_ability_header', component: CreateAbilityHeaderComponent },
  { path: 'login', component: LoginComponent },
  { path: 'search_information', component: SearchInformationComponent },
  { path: 'search_history', component: SearchHistoryComponent },
  { path: 'search_address', component: SearchAddressComponent },
  { path: 'edit_student', component: EditStudentComponent },
  { path: 'edit_parent/:id', component: EditParentComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    CreateStudentComponent,
    LoginComponent,
    SearchInformationComponent,
    ListKindergartenComponent,
    ListPrimarySchoolComponent,
    ListSecondarySchoolComponent,
    HeaderComponent,
    ProfileComponent,
    CreateParentComponent,
    CreateAddressComponent,
    CreateMotherComponent,
    CreateFatherComponent,
    FilterPipe,
    CreateAbilityComponent,
    ConfirmDialogComponent,
    GroupByPipe,
    ListAbilityHeaderComponent,
    CreateAbilityHeaderComponent,
    AbilityFilterPipe,
    AbilityHeaderFilterPipe,
    CreateAbilityDetailComponent,
    CreateAbilityFormComponent,
    AbilityListfilterPipe,
    AbilityListHeaderfilterPipe,
    SearchAddressComponent,
    FilterAddressPipe,
    FilterYearPipe,
    DevelopStudentComponent,
    DevelopStudentFormComponent,
    CreateHealthComponent,
    CreateHistoryStudentComponent,
    SearchHistoryComponent,
    SearchHistoryPipe,
    FilterYearAbilityPipe,
    AbilityFormProfileComponent,
    EditStudentComponent,
    EditAddressHomeComponent,
    EditCurrentAddressComponent,
    EditHealthComponent,
    EditParentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ProgressbarModule.forRoot(),
    ConfirmationPopoverModule.forRoot(),
    AccordionModule.forRoot()
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [AbilityService,
              AddressHomeParticularService,
              CrippleService,
              CurrentAddressService,
              FamilyMemberService,
              ParentService,
              StudentService,
              ListAbilityService,
              UploadImageService,
              CreateAddressComponent,
              HeaderComponent,
              AbilityHeaderService,
              AbilityDetailService,
              HealthService,
              HistoryStudentService,
              CreateStudentComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
