import { CrPanyanukulClientPage } from './app.po';

describe('cr-panyanukul-client App', () => {
  let page: CrPanyanukulClientPage;

  beforeEach(() => {
    page = new CrPanyanukulClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
